﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LinePosition : MonoBehaviour
{
    public Client currentClient;
    //bool rotated;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }
    private void Update()
    {
        if (currentClient != null)
        {
            if (Vector3.Distance(currentClient.transform.position, transform.position) < 0.1f)
            {
                if (Quaternion.Angle(currentClient.transform.rotation, transform.rotation) > 1)
                {
                    currentClient.transform.rotation = transform.rotation;
                    //StartCoroutine(RotateClient(currentClient.transform));
                }

            }
        }
    }
    IEnumerator RotateClient(Transform client)
    {
        Vector3 targ = transform.position;
        targ.z = 0f;
        Vector3 objectPos = client.position;
        targ.x = targ.x - objectPos.x;
        targ.y = targ.y - objectPos.y;
        float angle = Mathf.Atan2(targ.y, targ.x) * Mathf.Rad2Deg;
        //transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        while (Quaternion.Angle(client.rotation, transform.rotation)>1)
        {
            yield return new WaitForEndOfFrame();
            client.rotation = Quaternion.Euler((Vector3.up*Time.deltaTime) * angle);
        }
        //rotated = false;
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    NavMeshAgent agent = other.GetComponent<NavMeshAgent>();
    //    agent.updateRotation = false;


    //}
    //private void OnTriggerStay(Collider other)
    //{
    //    NavMeshAgent agent = other.GetComponent<NavMeshAgent>();
    //    if (agent.velocity.sqrMagnitude > Mathf.Epsilon)
    //    {
    //        transform.rotation = Quaternion.LookRotation(transform.forward, transform.up);
    //    }
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    NavMeshAgent agent = other.GetComponent<NavMeshAgent>();
    //    agent.updateRotation = true;
    //}
}
