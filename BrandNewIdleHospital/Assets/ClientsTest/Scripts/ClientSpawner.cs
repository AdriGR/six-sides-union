﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClientSpawner : MonoBehaviour
{
    public Client clientPrefab;
    Client clientInstance;
    GameObject[] clientPrefabs;
    public DecisionPoint entrance;
    [SerializeField] float minSpeed;
    [SerializeField] float maxSpeed;
    private void Awake()
    {
        
    }
    public void SpawnClient(RoomController destination)
    {
        clientPrefabs = Resources.LoadAll<GameObject>("Customers");
        clientInstance = GameObject.Instantiate(clientPrefab, transform.position, Quaternion.identity) as Client;
        GameObject clientMesh = GameObject.Instantiate(clientPrefabs[Random.Range(0, clientPrefabs.Length)], clientInstance.MeshLocator);
        clientMesh.transform.SetParent(clientInstance.MeshLocator);
        clientMesh.transform.localPosition = Vector3.zero;
        clientInstance.mySpawner = this;
        clientInstance.SetRoom(destination.navigation);
        if (transform.root.GetComponent<Car>() != null)
        {
            Car myCar = transform.root.GetComponent<Car>();
            if (myCar.destiny >= 1 && myCar.destiny < 4)
            {
                clientInstance.entrance = GameObject.Find("TerminusFromParkingA").GetComponent<DecisionPoint>();
            }
            else if (myCar.destiny >= 4 && myCar.destiny < 7)
            {
                clientInstance.entrance = GameObject.Find("TerminusFromParkingB").GetComponent<DecisionPoint>();
            }
            else if (myCar.destiny >= 7 && myCar.destiny < 10)
            {
                clientInstance.entrance = GameObject.Find("TerminusFromParkingA").GetComponent<DecisionPoint>();
            }
            else if (myCar.destiny >= 10 && myCar.destiny < 13)
            {
                clientInstance.entrance = GameObject.Find("TerminusFromParkingB").GetComponent<DecisionPoint>();
            }
            //else if (myCar.destiny >= 13 && myCar.destiny < 19)
            //{
            //    clientInstance.entrance = GameObject.Find("TerminusFromParkingC").GetComponent<DecisionPoint>();
            //}
        }
        else 
        {
            clientInstance.entrance = entrance;
        }
        
        clientInstance.SetSpeed(Random.Range(minSpeed, maxSpeed));
        clientInstance.SetTarget(clientInstance.entrance.transform.position);
    }
    public void SpawnClient()
    {
        clientPrefabs = Resources.LoadAll<GameObject>("Customers");
        clientInstance = GameObject.Instantiate(clientPrefab, transform.position, Quaternion.identity) as Client;
        GameObject clientMesh = GameObject.Instantiate(clientPrefabs[Random.Range(0, clientPrefabs.Length)], clientInstance.MeshLocator);
        clientMesh.transform.SetParent(clientInstance.MeshLocator);
        clientMesh.transform.localPosition = Vector3.zero;
        clientInstance.mySpawner = this;
        
        clientInstance.SetSpeed(Random.Range(minSpeed, maxSpeed));
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Client>() != null)
        {
            Client myClient = other.GetComponent<Client>();
            if (myClient.attended && myClient.mySpawner == this)
            {
                if (transform.GetComponentInParent<Car>() != null)
                {
                    Car myCar = transform.parent.parent.GetComponent<Car>();
                    myCar.StartCoroutine(myCar.ExitParkingAnim());
                    Destroy(other.gameObject);
                }
                else 
                {
                    Destroy(other.gameObject);
                }
                
            }
        }
    }
}
