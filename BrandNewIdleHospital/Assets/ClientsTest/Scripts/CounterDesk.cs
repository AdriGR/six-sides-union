﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CounterDesk : InteractionAfterClick
{
    [Range(1,5)]
    public int attendantsAmount;
    public int currentAttendants;
    public RoomNavigation navigation;
    public bool receptionDesk;
    private void Awake()
    {
        navigation = GetComponent<RoomNavigation>();
        SpawnManager spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        if (spawnManager != null && !receptionDesk)
        {
            //spawnManager.posibleDestinations.Add(this);
        }
    }
    public override void ExecuteFingerUp()
    {
        
        //startTemp = false;
        //if (cont <= desiredTime && !MoveCamera.move)
        //{
        //    LaunchMenu();
        //}
        LaunchMenu();
    }
    public override void Update()
    {
        if (attendantsAmount != currentAttendants)
        {
            currentAttendants = attendantsAmount;
            navigation.EnableAttendants(currentAttendants);
            
        }
        

        base.Update();

    }    
    void LaunchMenu()
    {
        canvas.enabled = true;
      
    }
   
}

