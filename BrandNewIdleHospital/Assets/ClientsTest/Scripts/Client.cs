﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Client : MonoBehaviour
{
    public enum ClientState { None, Angry, Happy }
    public ClientState myState;
    ClientState currentState;
    NavMeshAgent myAgent;
    public Transform path;
    public Transform currentPath;
    public Waypoint_Pedestrians currentWaypoint;
    public Waypoint_Pedestrians nextWaypoint;
    public float distanceToWaypoint;
    public bool returning;
    public bool attended;
    public bool inLinePosition;
    public ClientSpawner mySpawner;
    public Vector3 currentDestination;
    public RoomNavigation currentRoomNav;
    Animator anim;
    public Transform MeshLocator;
    public Address destinationAddress;
    public DecisionPoint entrance;
    ShowState stateDisplay;
    public bool waiting;
    public float timeToGetAngry;
    public float angryTimer;
    public Money currentSpend;
    private void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        SetTarget(entrance.transform.position);
        stateDisplay = GetComponentInChildren<ShowState>();
        currentState = myState;
        currentSpend = new Money();
    }
    public void SetRoom(RoomNavigation nav)
    {
        currentRoomNav = nav;
        destinationAddress = nav.myAddress;
        
    }
    public void SetTarget(Vector3 destination)
    {
        if (myAgent == null)
        {
            myAgent = GetComponent<NavMeshAgent>();
        }
        myAgent.SetDestination(destination);
        currentDestination = destination;
    }
    public void SetSpeed(float desiredSpeed)
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAgent.speed = desiredSpeed;
    }
    Waypoint_Pedestrians SetNextWaypoint(Waypoint_Pedestrians currentPathWaypoint)
    {
        Waypoint_Pedestrians next = null;
        
        if (next == null)
        {
            if (returning)
            {
               return currentPathWaypoint.previousWaypoint;
            }
            else
            {
                return currentPathWaypoint.nextWaypoint;
            }
        }
        return null;
        
        
    }
   
    public void StartPath()
    {
        if (returning)
        {
            Waypoint_Pedestrians otherWaypoint = currentPath.GetChild(currentPath.childCount - 1).GetComponent<Waypoint_Pedestrians>();
            SetTarget(otherWaypoint.transform.position + transform.right*Random.Range(-otherWaypoint.width/2, otherWaypoint.width/2));
            currentWaypoint = currentPath.GetChild(currentPath.childCount-1).GetComponent<Waypoint_Pedestrians>();
        }
        else 
        {
            Waypoint_Pedestrians otherWaypoint = currentPath.GetChild(0).GetComponent<Waypoint_Pedestrians>();
            SetTarget(otherWaypoint.transform.position + transform.right * Random.Range(-otherWaypoint.width / 2, otherWaypoint.width / 2));
            currentWaypoint = currentPath.GetChild(0).GetComponent<Waypoint_Pedestrians>();
        }
        
    }
    
    public void SetAttended(RoomController desk)
    {
        RoomData data = desk.GetComponent<RoomData>();
        currentSpend.AddMoney(data.currentIncome);
        
        attended = true;
        returning = true;
        destinationAddress = currentRoomNav.nextAddress;
        SetTarget(currentRoomNav.exitPoint.position);
        


    }
    public void PayAndLeave()
    {

        GameData.currentData.money.AddMoney(currentSpend);
        attended = true;
        returning = true;
        destinationAddress = entrance.address;
        SetTarget(currentRoomNav.exitPoint.position);
    }
    void ChangeState(ClientState state)
    {
        myState = state;
        currentState = state;
        stateDisplay.StartCoroutine(stateDisplay.ShowMyState(state));
        
    }
    void SetAngry()
    {
        ChangeState(ClientState.Angry);
        attended = true;
        returning = true;
        currentRoomNav.waitingClients.Remove(this);
        SetTarget(currentRoomNav.exitPoint.position);
        destinationAddress = entrance.address;
        

    }
    public void BeingAttendedBy(BaseAttendant att)
    {
        SetTarget(att.transform.Find("ClientPoint").position);
    }
    private void Update()
    {
        if (anim)
        {
            anim.SetFloat("WalkingFloat", myAgent.velocity.magnitude);
        }
        
        if (myAgent.destination != null)
        {
            myAgent.transform.GetChild(0).LookAt(myAgent.nextPosition);
        }
        if (currentWaypoint != null)
        {
            distanceToWaypoint = Vector3.Distance(transform.position, currentWaypoint.transform.position);
            if (currentPath != null && currentWaypoint != null && distanceToWaypoint < currentWaypoint.width)
            {
                
                nextWaypoint = SetNextWaypoint(currentWaypoint);
                if (nextWaypoint != null)
                {
                    SetTarget(nextWaypoint.transform.position + transform.right * Random.Range(-nextWaypoint.width / 2, nextWaypoint.width / 2));
                    currentWaypoint = nextWaypoint;
                    nextWaypoint = null;
                }
            }
        }
        if (waiting)
        {
            if (angryTimer == 0)
            {
                angryTimer = timeToGetAngry;
            }
            else if (angryTimer > 0)
            {
                angryTimer -= Time.deltaTime;
            }
            else if (angryTimer < 0)
            {
                SetAngry();
                angryTimer = 0;
                waiting = false;
            }
        }
        else 
        {
            angryTimer = 0;
            waiting = false;
        }
        if (currentState != myState)
        {
            ChangeState(myState);

        }
    }

}
