﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public float spawnRate;
    public float currentTimer;
    public ClientSpawner[] spawners;
    //public List<RoomController> posibleDestinations = new List<RoomController>();
    public RoomController[] posibleDestinations;
    private void Start()
    {
        
    }

    void Update()
    {
        if (currentTimer > 0)
        {
            currentTimer -= Time.deltaTime;
        }
        else
        {
            currentTimer = spawnRate;
            if (posibleDestinations.Length > 0)
            {
                SpawnAClient();
            }

        }
    }
    void SpawnAClient()
    {
        posibleDestinations = GameObject.FindObjectsOfType<RoomController>();
        if (posibleDestinations.Length > 0)
        {
            RoomController actualDestination = posibleDestinations[Random.Range(0, posibleDestinations.Length)];
            if (spawners.Length > 0)
            {
                spawners[Random.Range(0, spawners.Length)].SpawnClient(actualDestination);
            }
        }
        
        
    }
    public void SpawnAClient(ClientSpawner spawner)
    {
        posibleDestinations = GameObject.FindObjectsOfType<RoomController>();
        RoomController actualDestination = posibleDestinations[Random.Range(0, posibleDestinations.Length)];
        spawner.SpawnClient(actualDestination);
        
    }
}
