﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeLineChecker : MonoBehaviour
{
    public RoomNavigation myRoomNav;
    
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.GetComponent<Client>() != null)
        {
            
            Client currentClient = other.GetComponent<Client>();
            
            if (!currentClient.attended)
            {
                
                currentClient.SetRoom(myRoomNav);
                if (myRoomNav.clientsInLine.Count < myRoomNav.myLinePositions.Length)
                {
                    Vector3 lineDest;
                    myRoomNav.clientsInLine.Add(currentClient);
                    lineDest = myRoomNav.myLinePositions[myRoomNav.clientsInLine.Count-1].transform.position;
                    currentClient.SetTarget(lineDest);
                }
                else
                {
                    Vector3 waitDest;
                    myRoomNav.waitingClients.Add(currentClient);
                    Vector2 waitPosition = Random.insideUnitCircle * myRoomNav.waitingAreaRadius;
                    waitDest = new Vector3(myRoomNav.waitPoint.position.x + waitPosition.x, myRoomNav.waitPoint.position.y, myRoomNav.waitPoint.position.z + waitPosition.y);
                    currentClient.waiting = true;
                    currentClient.SetTarget(waitDest);
                }
                
                
                
            }
            
        }
    }

}
