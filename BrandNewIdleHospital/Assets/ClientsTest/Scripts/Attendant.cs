﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Attendant : BaseAttendant
{
    
    
    
    RoomController myDesk;
    public override void Start()
    {
        base.Start();
        myDesk = transform.GetComponentInParent<RoomController>();
        //speed = myDesk.currentAttendSpeed;
    }
    
    
    public override void AttendingComplete()
    {
        timer = 0;
        myBar.fillAmount = 0;
        calling = false;
        attendingClient = false;
        currentlyAttending.SetAttended(myDesk);
        currentlyAttending = null;
        //myDesk.NextClient(this);
        //Debug.Log("Client Attended in desk " + transform.root.name+" --- DINERITO!!!!");
        
        //GiveMoney
    }
}
