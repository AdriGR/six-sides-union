﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PathEditorWindow : EditorWindow
{
    [MenuItem("Tools/Path Editor")]
    public static void Open()
    {
        GetWindow<PathEditorWindow>();
    }

    public Transform waypointRoot;

    private void OnGUI()
    {
        SerializedObject obj = new SerializedObject(this);
        EditorGUILayout.PropertyField(obj.FindProperty("waypointRoot"));
        if (waypointRoot == null)
        {
            EditorGUILayout.HelpBox("Root transform must be selected.", MessageType.Warning);
        }
        else 
        {
            EditorGUILayout.BeginVertical("box");
            DrawButtons();
            EditorGUILayout.EndVertical();
        }
        obj.ApplyModifiedProperties();
    }
    void DrawButtons()
    {
       
        if (waypointRoot.GetComponent<PathControl>() == null)
        {
            if (GUILayout.Button("Prepare Path"))
            {
                PreparePath();
            }
        }
        if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<PathControl>() != null)
        {
            GUILayout.Label("PATH");
            if (GUILayout.Button("Create Waypoint"))
            {
                CreateWaypoint();
            }
            if (GUILayout.Button("Create Terminus"))
            {
                CreateTerminus();

            }
        }
        
        if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Waypoint_Pedestrians>() != null)
        {
            GUILayout.Label("WAYPOINT");
            if (GUILayout.Button("Create Waypoint Before"))
            {
                CreateWaypointBefore();
            }
            if (GUILayout.Button("Create Waypoint After"))
            {
                CreateWaypointAfter();
            }
            if (GUILayout.Button("Remove Waypoint"))
            {
                RemoveWaypoint();
            }
            
        }
        if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<DecisionPoint>() != null)
        {
            GUILayout.Label("TERMINUS");
        }
    }
    void PreparePath()
    {
        PathControl control = waypointRoot.gameObject.AddComponent<PathControl>();

        waypointRoot.position = new Vector3(waypointRoot.position.x, 0.0f, waypointRoot.position.z);
        CreateWaypoint();
    }
    void CreateWaypoint()
    {
        GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint_Pedestrians));
        waypointObject.transform.SetParent(waypointRoot, false);
        Waypoint_Pedestrians myWaypoint = waypointObject.GetComponent<Waypoint_Pedestrians>();
        if (waypointRoot.childCount > 1)
        {
            myWaypoint.previousWaypoint = waypointRoot.GetChild(waypointRoot.childCount - 2).GetComponent<Waypoint_Pedestrians>();
            myWaypoint.previousWaypoint.nextWaypoint = myWaypoint;
            myWaypoint.transform.position = myWaypoint.previousWaypoint.transform.position;
            myWaypoint.transform.forward = myWaypoint.previousWaypoint.transform.forward;
        }
        Selection.activeGameObject = myWaypoint.gameObject;
    }
    void CreateWaypointBefore()
    {
        GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint_Pedestrians));
        waypointObject.transform.SetParent(waypointRoot, false);
        Waypoint_Pedestrians newWaypoint = waypointObject.GetComponent<Waypoint_Pedestrians>();
        Waypoint_Pedestrians selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoint_Pedestrians>();
        waypointObject.transform.position = selectedWaypoint.transform.position;
        waypointObject.transform.forward = selectedWaypoint.transform.forward;
        if (selectedWaypoint.previousWaypoint != null)
        {
            newWaypoint.previousWaypoint = selectedWaypoint.previousWaypoint;
            selectedWaypoint.previousWaypoint.nextWaypoint = newWaypoint;
        }
        newWaypoint.nextWaypoint = selectedWaypoint;
        selectedWaypoint.previousWaypoint = newWaypoint;
        newWaypoint.transform.SetSiblingIndex(selectedWaypoint.transform.GetSiblingIndex());
        Selection.activeGameObject = newWaypoint.gameObject;
    }
    void CreateWaypointAfter()
    {
        GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint_Pedestrians));
        waypointObject.transform.SetParent(waypointRoot, false);
        Waypoint_Pedestrians newWaypoint = waypointObject.GetComponent<Waypoint_Pedestrians>();
        Waypoint_Pedestrians selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoint_Pedestrians>();
        waypointObject.transform.position = selectedWaypoint.transform.position;
        waypointObject.transform.forward = selectedWaypoint.transform.forward;
        if (selectedWaypoint.nextWaypoint != null)
        {
            newWaypoint.nextWaypoint = selectedWaypoint.nextWaypoint;
            selectedWaypoint.nextWaypoint.previousWaypoint = newWaypoint;
        }
        newWaypoint.previousWaypoint = selectedWaypoint;
        selectedWaypoint.nextWaypoint = newWaypoint;
        newWaypoint.transform.SetSiblingIndex(selectedWaypoint.transform.GetSiblingIndex());
        Selection.activeGameObject = newWaypoint.gameObject;
    }
    void RemoveWaypoint()
    {
        Waypoint_Pedestrians selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoint_Pedestrians>();
        if (selectedWaypoint.nextWaypoint != null)
        {
            selectedWaypoint.nextWaypoint.previousWaypoint = selectedWaypoint.previousWaypoint;
        }
        if (selectedWaypoint.previousWaypoint != null)
        {
            selectedWaypoint.previousWaypoint.nextWaypoint = selectedWaypoint.nextWaypoint;
            Selection.activeGameObject = selectedWaypoint.previousWaypoint.gameObject;
        }
        DestroyImmediate(selectedWaypoint.gameObject);
    }
    void CreateTerminus()
    {
        GameObject terminus = new GameObject("Terminus " + waypointRoot.name, typeof(DecisionPoint));
        DecisionPoint terminusScript = terminus.gameObject.GetComponent<DecisionPoint>();
        
        terminus.transform.SetParent(waypointRoot, false);
        //int index = 0;// waypointRoot.transform.childCount - 1;
        
        waypointRoot.GetComponent<PathControl>().terminusB = terminus.GetComponent<DecisionPoint>();
        terminus.GetComponent<DecisionPoint>().terminusTo.Add(waypointRoot.GetComponent<PathControl>());
        Selection.activeGameObject = terminus.gameObject;
    }
}
