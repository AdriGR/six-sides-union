﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShowState : MonoBehaviour
{
    public Sprite[] statesSprites;
    SpriteRenderer myRenderer;
    
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(Camera.main.transform);
    }
    public IEnumerator ShowMyState(Client.ClientState state)
    {
        myRenderer.transform.localScale = Vector3.zero;
        myRenderer.sprite = statesSprites[(int)state];
        Tween firstTween = myRenderer.transform.DOScale(Vector3.one, 1).SetEase(Ease.OutElastic);
        yield return firstTween.WaitForCompletion();
        yield return new WaitForSeconds(3);
        StartCoroutine(HideMyState());
    }
    public IEnumerator HideMyState()
    {
        Tween firstTween = myRenderer.transform.DOScale(Vector3.zero, 1).SetEase(Ease.InElastic);
        yield return firstTween.WaitForCompletion();
        
    }
}
