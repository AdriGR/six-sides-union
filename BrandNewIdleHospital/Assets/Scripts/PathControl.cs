﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathControl : MonoBehaviour
{

    public DecisionPoint terminusA;
    public DecisionPoint terminusB;
    public Waypoint_Pedestrians[] waypointsPath;
    private void Awake()
    {
        waypointsPath = GetComponentsInChildren<Waypoint_Pedestrians>();
    }
}
