using System;
using System.Collections.Generic;
using UnityEngine;

public class ReceptionController : InteractionAfterClick
{
    public Cashier[] cashiers;
    public RoomNavigation navigation;
    public int cashiersAmount;
    public int currentCashiers;
    public Money addCashierBase;
    public Money addCashierCurrentCost;
   
    public override void ExecuteFingerUp()
    {
        canvas.enabled = true;
        ReceptionMenuController receptionMenu = canvas.GetComponent<ReceptionMenuController>();
        
        receptionMenu.Initialize(this);
    }

    void Start()
    {
        navigation = GetComponent<RoomNavigation>();
        addCashierCurrentCost = CalculateNextCashierCost(currentCashiers);
        navigation.EnableAttendants(currentCashiers);
        canvas = GameObject.FindObjectOfType<ReceptionMenuController>().GetComponent<Canvas>();
    }


    public override void Update()
    {
        base.Update();
        //if (cashiersAmount != currentCashiers)
        //{
        //    currentCashiers = cashiersAmount;
        //    navigation.EnableAttendants(currentCashiers);

        //}
    }
    public void AddCashier()
    {
        if (currentCashiers <= 2) //Limita a 3 el n�mero de cashiers
        {
            GameData.currentData.money.SubstractMoney(addCashierCurrentCost);
            currentCashiers += 1;
            navigation.EnableAttendants(currentCashiers);
            addCashierCurrentCost = CalculateNextCashierCost(currentCashiers);
        }
        else 
        {
            Debug.Log("Cashiers amount limited to 3. Go to 'ReceptionController.AddCashier()' to remove the limitation");
        }

    }
    Money CalculateNextCashierCost(int currentCashiersAmount)
    {
        //(PrecioActual * attendantsAmount) ^ 1.1;
        double tempMoney = 0;
        if (currentCashiersAmount > 1)
        {
            double a = addCashierCurrentCost.moneyAmount * currentCashiersAmount;
            tempMoney = Math.Pow(a, 1.1);

        }
        else
        {
            tempMoney = addCashierBase.moneyAmount;

        }

        Money result = new Money(tempMoney);
        return result;
    }
}
