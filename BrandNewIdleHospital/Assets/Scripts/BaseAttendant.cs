using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseAttendant : MonoBehaviour
{
    public Client currentlyAttending;
    public float speed;
    public float baseAttendingTime;
    public float timer = 0;
    public bool calling;
    public bool attendingClient;
    public Image myBar;
    public virtual void Start()
    {
        timer = 0;
        myBar.fillAmount = timer / baseAttendingTime;
        myBar.GetComponentInParent<Canvas>().worldCamera = Camera.main;
    }

    public virtual void Update()
    {
        if (currentlyAttending != null)
        {
            if (!calling)
            {

                NextClient(currentlyAttending);
            }
            else
            {
                if (Vector3.Distance(currentlyAttending.transform.position, transform.Find("ClientPoint").position) < 0.1f)
                {
                    if (Quaternion.Angle(currentlyAttending.transform.rotation, transform.Find("ClientPoint").rotation) > 10)
                    {
                        //StartCoroutine(RotateClient(currentlyAttending.transform));
                        currentlyAttending.transform.rotation = transform.Find("ClientPoint").rotation;
                    }
                    
                    attendingClient = true;
                }
            }
        }
        if (attendingClient)
        {
            //currentlyAttending.transform.LookAt(transform);
            if (timer < baseAttendingTime)
            {
                timer += Time.deltaTime * speed;
                myBar.fillAmount = timer / baseAttendingTime;
            }
            else
            {
                AttendingComplete();
            }
        }


    }
    IEnumerator RotateClient(Transform client)
    {
        Transform lookAtMe = transform.Find("ClientPoint");
        while (Quaternion.Angle(client.rotation, lookAtMe.rotation) > 10)
        {
            yield return new WaitForEndOfFrame();
            client.rotation *= Quaternion.Euler(Vector3.up * Time.deltaTime);
        }
    }
    public void NextClient(Client attent)
    {
        attent.BeingAttendedBy(this);
        calling = true;
        attendingClient = true;
        
    }
    public abstract void AttendingComplete();
}
