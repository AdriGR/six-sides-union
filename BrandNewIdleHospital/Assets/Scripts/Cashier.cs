using System;
using System.Collections.Generic;
using UnityEngine;

public class Cashier : BaseAttendant
{
    public int currentSpeedLevel;
    public float baseCashierSpeed;
    public float speedIncrementStep;
    public float customersPerMinute;
    public Money baseCashierSpeedUpdateCost;
    public Money currentNextCashierSpeedUpdateCost;
    public override void Start()
    {
        base.Start();        
        
        SetSpeed(currentSpeedLevel);
        
    }
    public override void Update()
    {
        base.Update();
        //if (desiredSpeedLevel != currentSpeedLevel)
        //{
        //    currentSpeedLevel = desiredSpeedLevel;
        //    speed = CalculateCurrentSpeed(currentSpeedLevel, baseCashierSpeed, speedIncrementStep);
        //    currentNextCashierSpeedUpdateCost = CalculateNextSpeedCost(currentSpeedLevel, currentNextCashierSpeedUpdateCost);
        //}
        
    }
    public override void AttendingComplete()
    {
        timer = 0;
        myBar.fillAmount = 0;
        calling = false;
        attendingClient = false;
        currentlyAttending.PayAndLeave();
        currentlyAttending = null;
    }
    float CalculateCurrentSpeed(int speedLevel, float baseSpeed, float increment)
    {
        float result = 0.0f;
        result = baseSpeed + (speedLevel * increment);
        customersPerMinute = result;
        return result;
    }

    public void SetSpeed(int speedLevel)
    {
        speed = CalculateCurrentSpeed(speedLevel, baseCashierSpeed, speedIncrementStep);
        currentNextCashierSpeedUpdateCost = CalculateNextSpeedCost(speedLevel, currentNextCashierSpeedUpdateCost);
    }
    public void UpgradeSpeed()
    {
        if (GameData.currentData.money.IsEnough(currentNextCashierSpeedUpdateCost))
        {
            GameData.currentData.money.SubstractMoney(currentNextCashierSpeedUpdateCost);
            currentSpeedLevel += 1;
            SetSpeed(currentSpeedLevel);
        }
        
    }
    Money CalculateNextSpeedCost(int currentLevel, Money currentCost)
    {
        //Precio Actual ^ 1.1
        Money tempMoney = new Money();
        if (currentLevel > 1)
        {
            tempMoney.moneyAmount = Math.Pow(currentCost.moneyAmount,1.1);
        }
        else
        {
            tempMoney.moneyAmount = baseCashierSpeedUpdateCost.moneyAmount;

        }

        Money result = new Money(tempMoney.moneyAmount);
        return result;
    }
}
