﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpenDoors : MonoBehaviour
{
    List<Client> inDoorSensor = new List<Client>();
    public Transform leftDoor;
    public Transform rightDoor;
    public float openDistance;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Client>() != null)
        {
            inDoorSensor.Add(other.GetComponent<Client>());
        }
    }
    private void Update()
    {
        if (inDoorSensor.Count > 0)
        {
            OpenMyDoors();
        }
        else 
        {
            CloseMyDoors();
        }
    }
    void OpenMyDoors()
    {
        //GetComponent<Animator>().SetBool("Open", true);
        rightDoor.DOLocalMoveX(openDistance*-1f, 1f);
        leftDoor.DOLocalMoveX(openDistance, 1f);
    }
    void CloseMyDoors()
    {
        //GetComponent<Animator>().SetBool("Open", false);
        rightDoor.DOLocalMoveX(0f, 1f);
        leftDoor.DOLocalMoveX(0f, 1f);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Client>() != null)
        {
            inDoorSensor.Remove(other.GetComponent<Client>());
        }
    }
}
