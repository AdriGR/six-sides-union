﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysLookAtCam : MonoBehaviour
{
    
    void Update()
    {
        transform.LookAt(transform.position+Vector3.forward+(Vector3.right*-0.8f));
    }
}
