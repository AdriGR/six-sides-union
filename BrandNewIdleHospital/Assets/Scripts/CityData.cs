using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityData : MonoBehaviour
{

    public float limitZForward, limitZBackward;
    public float limitXRight, limitXLeft;
    public float limitYUp, limitYDown;

}
