﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    // número del parking
    public int[] sites;

    // Para indicar el waypoint para salir de la fila del aparcamiento
    public Transform exit;
    public Transform entrySiteWP;
    public bool right;

    // Start is called before the first frame update
    void Start()
    {
        entrySiteWP = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Car"))
        {
            // Recuperamos el componente el objeto colisionado
            Car tempCar = other.GetComponent<Car>();

            // Recorremos el array
            for(int i = 0; i < sites.Length; i++)
            {
                // Si el destino del coche está en el array, devolvemos el transform que ocupa
                if(tempCar.destiny == sites[i])
                {
                    //tempCar.state = Car.State.FindParking;
                    // Indicamos al coche el transform del aparcamiento asignado
                    //tempCar.parkingWaypoint = CarParkingManager.instance.FindParking(sites[i]);  

                    // Le indicamos cuál será el waypoint de salida de esa calle una vez salga del aparcamiento
                    tempCar.nextWaypoint = entrySiteWP;
                    tempCar.exitSite = exit;
                    

                    // Le decimos si tiene que girar a la izquierda o a la derecha
                    //tempCar.right = right;

                    
                }
            }
        }
    }
}
