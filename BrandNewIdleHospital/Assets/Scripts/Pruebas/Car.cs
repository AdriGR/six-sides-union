﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Namespace para poder utilizar DoTween
using DG.Tweening;
using UnityEngine.AI;


public class Car : MonoBehaviour
{
    [Header("Efectos")]
    // Partículas de humo
    public ParticleSystem smokeParticles;

    // Animación de moverse
    [SerializeField] private Animator animator;

    [Header("Aparcar")]
    // Posiciones para realizar la animación de aparcar
    public Vector3[] path;

    // Tiempo que tardará en completarse la animación
    public float duration;

    Tween animTween;
    public Transform body;
    // Enumerador que indica el estado actual del vehículo
    public enum State { Driving, FindParking, EntrySite, ExitSite}

    [Header("Estados")]
    // Referencia al enumerador para poder cambiar el estado
    public State state;

    // Velocidad a la que se movera el coche
    [SerializeField] private float speed;

    // Destino del vehículo donde tiene que aparcar
    public int destiny;

    // Distancia mínima para cambiar de waypoint
    public float minDistanceToChangeWaypoint = 0.2f;

    // Para indicar si ha aparcado
    public bool isParked;

    // Transform que indicará la salida de la calle del aparcamiento
    public Transform exitSite;

    // Waypoint al que nos estamos dirigiendo
    public Transform nextWaypoint;

    // Waypoint de aparcamiento que será pasado en el script checker
    public Transform parkingWaypoint;

    public ClientSpawner mySpawner;

    public ControlPanelParking controlPanelParking;

    Vector3 velocity = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
        // Recuperamos componentes
        animator = GetComponent<Animator>();
        controlPanelParking = CarParkingManager.instance.currentParking.GetComponentInChildren<ControlPanelParking>();

        // Le pedimos al mánager del parking un sitio libre y la ruta a seguir
        destiny = CarParkingManager.instance.SearchParking();
        parkingWaypoint = CarParkingManager.instance.FindParking(destiny);
        CarParkingManager.instance.carsInGame.Add(this);
        animator.SetBool("move", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Driving)
        {
            Drive();
 
        }else if (state == State.FindParking)
        {
            FindParking();
        } else if(state == State.EntrySite)
        {
            //StartCoroutine(EntryParkingAnim());
        } /*else if(state == State.ExitSite)
        {
            StartCoroutine(ExitParkingAnim());
        }*/

        //if (Input.GetButtonDown("Jump"))
        //{
        //    Debug.Log("Cambiar por la orden de que el cliente llegó");
        //    StartCoroutine(ExitParkingAnim());
        //}
    }

    /// <summary>
    /// Método para ir a través de los waypoints que será llamado desde los waypoints, que le indicarán donde ir
    /// </summary>
    /// <param name="nextWaypoint"></param>
    public void Drive()
    {
        
        // Nos movemos hacia el siguiente waypoint y miramos hacia el
        transform.position = Vector3.MoveTowards(transform.position, nextWaypoint.position, speed * Time.deltaTime);
        
        transform.LookAt(nextWaypoint.transform);
        
        // Si la distancia al waypoint es menor que la indicada, intentamos recoger su componente y su siguiente waypoint, de no tener destruimos el vehículo y lo quitamos de la lista
       if(Vector3.Distance(transform.position, nextWaypoint.position) < minDistanceToChangeWaypoint)
        {
            nextWaypoint = nextWaypoint.GetComponent<Waypoint>().nextWaypoint;
            
            if (nextWaypoint == null)
            {
                Destroy(gameObject);
                CarParkingManager.instance.carsInGame.Remove(this);
                
            } else if(nextWaypoint.gameObject.CompareTag("DecisionWP"))
            {
                nextWaypoint = nextWaypoint.GetComponent<Waypoint>().DecisionToPark(parkingWaypoint);
                state = State.FindParking;
            }
      
        }
    }

    /// <summary>
    /// Método para "buscar" el parking asignado, realmente nos dirigimos hacia el sitio
    /// </summary>
    private void FindParking()
    {
        transform.position = Vector3.MoveTowards(transform.position, nextWaypoint.position, speed * Time.deltaTime);
        transform.LookAt(nextWaypoint.position);
        if (Vector3.Distance(transform.position, nextWaypoint.position) < minDistanceToChangeWaypoint)
        {
            

           
            // Dejamos de movernos
            speed = 0f;

            state = State.EntrySite;
            StartCoroutine(EntryParkingAnim());
        }
    }

    /// <summary>
    /// Método que será llamado para realizar la entrada al parking
    /// </summary>
    /// <returns></returns>
    private IEnumerator EntryParkingAnim()
    {        
        yield return new WaitForSeconds(0.25f);

        
        //List<Transform> tempPath = new List<Transform>();
        int sizeTempPath = parkingWaypoint.childCount;
        /*for(int i = 0; i < sizeTempPath; i++)
        {
            tempPath.Add(parkingWaypoint.GetChild(i));
        }*/

        path = new Vector3[sizeTempPath];
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = parkingWaypoint.GetChild(i).position;
            }
            animTween = transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.2f).SetAutoKill(false);
        
        

        yield return animTween.WaitForCompletion();

        isParked = true;
        smokeParticles.Stop();
        animator.SetBool("move", false);
        controlPanelParking.SubtractIndicator();
        GameObject.Find("SpawnManager").GetComponent<SpawnManager>().SpawnAClient(mySpawner);
        
    }

    /// <summary>
    /// Corrutina para salir del parking
    /// </summary>
    /// <returns></returns>
    public IEnumerator ExitParkingAnim()
    {
        /*
        int sizeTempPath = parkingWaypoint.childCount;
        path = new Vector3[sizeTempPath];
        //int cont = 0;
        for(int i = 0; i < path.Length; i++)
        {
            path[i] = parkingWaypoint.GetChild(i).position;
            //cont++;
        }

        animTween = transform.DOPath(path, duration, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.2f);
        */
        //yield return animTween.WaitForCompletion();
        
        
        animTween.PlayBackwards();

        yield return animTween.WaitForRewind();
        
        smokeParticles.Play();
        animator.SetBool("move", true);

        nextWaypoint = exitSite;
        speed = 8f;
        state = State.Driving;
        
        isParked = false;
        CarParkingManager.instance.FreeSite(destiny);
        controlPanelParking.AddIndicator();
        //GameObject.Find("InstantiateCar").GetComponent<InstantiateCar>().evento.Invoke();
        
    }

    /// <summary>
    /// Resetea la posición del modelo hijo al centro del padre
    /// </summary>
    public void Reset()
    {
        transform.GetChild(0).transform.localPosition = new Vector3(0f, 0f, 0f);
    }

}
