using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TypeInteraction : MonoBehaviour
{
    public enum Type {None, Ambulance, Office, CheckOut, Room, Parking}

    public Type type;


    public abstract void ExecuteInteraction();

    
    
}
