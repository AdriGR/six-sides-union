using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractionAfterClick : TypeInteraction
{
    public Canvas canvas;
    public float timer;
    public float cont;
    public float desiredTime;
    public bool startTemp;

    public virtual void Update()
    {
        
        //if (startTemp)
        //{
        //    InitTemp();
        //}

        //if (Input.GetButtonUp("Fire1") && cont <= desiredTime && !MoveCamera.move)
        //{
        //    ExecuteFingerUp();
        //}

    }

    public abstract void ExecuteFingerUp();

    public override void ExecuteInteraction()
    {
        if (!MoveCamera.move)
        {
            ExecuteFingerUp();
        }
        
        //cont = timer;
        //startTemp = true;
    }

    public void InitTemp()
    {
        //cont += Time.deltaTime;
    }
}
