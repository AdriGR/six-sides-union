using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlPanelParking : InteractionAfterClick
{
    // Sitios libres que quedan
    public int IndicatorParking;

    // Referencia al n�mero de sitios libres que se mostrar�n en el totem
    public TextMeshProUGUI indicatorParkingTXT;


    private void Awake()
    {
        // Recuperamos componente del canvas
        indicatorParkingTXT = transform.Find("Canvas").transform.Find("IndicatorSitesTXT").GetComponent<TextMeshProUGUI>();

        // Lo igualamos a la capacidad del parking y lo mostramos por pantalla
        IndicatorParking = CarParkingManager.instance.capacity;
        indicatorParkingTXT.text = IndicatorParking.ToString();
    }

    void Start()
    {
        // Iniciamos el proceso para rellenar los datos del parking
        CarParkingManager.instance.Init();

        // Recuperamos el componente canvas del men�
        canvas = GameObject.FindObjectOfType<UIParking>().GetComponent<Canvas>();

        // Recorremos la lista de coches y recuperamos su referencia perdida y los movemos a los sitios adecuados.
        foreach(Car car in CarParkingManager.instance.carsInGame)
        {
            CarParkingManager.instance.MoveCars(car);
            CarParkingManager.instance.FindMissingReferences(car);
        }
        
    }

    public override void ExecuteFingerUp()
    {
        canvas.enabled = true;
    }

    /// <summary>
    /// M�todo para sumar un sitio al indicador
    /// </summary>
    public void AddIndicator()
    {
        // Si el indicador es mayor o igual a la capacidad actual del parking, salimos
        if(IndicatorParking >= CarParkingManager.instance.capacity)
        {
            return;
        }

        // Sumamos un sitio y actualizamos el marcador
        IndicatorParking++;
        indicatorParkingTXT.text = IndicatorParking.ToString();
    }

    /// <summary>
    /// M�todo para restar un sitio al indicador
    /// </summary>
    public void SubtractIndicator()
    {
        // Si el indicador es menor o igual a 0, salimos
        if(IndicatorParking <= 0)
        {
            return;
        }

        // Restamos un sitio y actualizamos el marcador
        IndicatorParking--;
        indicatorParkingTXT.text = IndicatorParking.ToString();
    }

    /// <summary>
    /// M�todo para actualizar el indicador cuando se upgradee el parking
    /// </summary>
    public void UpdateIndicator()
    {
        IndicatorParking -= CarParkingManager.instance.carsInGame.Count;
        indicatorParkingTXT.text = IndicatorParking.ToString();
    }
}
