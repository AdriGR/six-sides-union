using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour
{
    private Camera cam;

    // Contador para ejecutar las acciones necesarias
    [SerializeField] private float timePressing;

    // Para saber que layers debe detectar el raycast
    public LayerMask interactableMask;

    [SerializeField] private bool isInteractable;


    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            DetectCollision();
        }

    }

    /// <summary>
    /// M�todo para detectar las colisiones con las ambulancias o con las salas
    /// </summary>
    private void DetectCollision()
    {
        // Lanzamos un rayo desde la posici�n del rat�n hacia el mundo
        Ray camRay = cam.ScreenPointToRay(Input.mousePosition);

        // Para almacenar el resultado del raycast
        RaycastHit hitInfo = new RaycastHit();
        isInteractable = Physics.Raycast(camRay, out hitInfo, 1000f, interactableMask);
#if UNITY_EDITOR
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (isInteractable)
            {
                TypeInteraction tempType = hitInfo.collider.gameObject.GetComponent<TypeInteraction>();
                /*
                if(tempType != null && tempType.type == TypeInteraction.Type.Ambulance)
                {
                    Ambulance tempAmbulance = hitInfo.collider.gameObject.GetComponent<Ambulance>();

                    if(tempAmbulance != null)
                    {
                        StartCoroutine(tempAmbulance.ExitParking());
                    }
                }*/
                Debug.Log("he tocado a: " + hitInfo.collider.gameObject.name);
                tempType.ExecuteInteraction();

            }
        }
        else if(isInteractable && hitInfo.collider.GetComponent<ControlPanelParking>() != null)
        {
            TypeInteraction tempType = hitInfo.collider.gameObject.GetComponent<TypeInteraction>();
            
            Debug.Log("he tocado a: " + hitInfo.collider.gameObject.name);
            tempType.ExecuteInteraction();
        }
#elif UNITY_ANDROID
        if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            if (isInteractable)
            {
                TypeInteraction tempType = hitInfo.collider.gameObject.GetComponent<TypeInteraction>();
                /*
                if(tempType != null && tempType.type == TypeInteraction.Type.Ambulance)
                {
                    Ambulance tempAmbulance = hitInfo.collider.gameObject.GetComponent<Ambulance>();

                    if(tempAmbulance != null)
                    {
                        StartCoroutine(tempAmbulance.ExitParking());
                    }
                }*/
                Debug.Log("he tocado a: " + hitInfo.collider.gameObject.name);
                tempType.ExecuteInteraction();

            }
        }
        else if (isInteractable && hitInfo.collider.GetComponent<ControlPanelParking>() != null)
        {
            TypeInteraction tempType = hitInfo.collider.gameObject.GetComponent<TypeInteraction>();

            Debug.Log("he tocado a: " + hitInfo.collider.gameObject.name);
            tempType.ExecuteInteraction();
        }
#endif







    }
}
