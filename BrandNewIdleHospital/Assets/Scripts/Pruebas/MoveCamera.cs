﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveCamera : MonoBehaviour
{
    //Referencia a la cámara
    private Camera cam;
    
    // Velocidad de desplazamiento de la cámara
    [SerializeField] private float speed;
    // Fuerza de frenado 
    [SerializeField] private float brakeForce;
    [SerializeField] private float velocity;
    public float minVelocity;
    private Vector3 startTouch;
    private Vector3 direction;
    private Vector3 previousPosition;
    // Para saber si se está moviendo con la fuerza del desplazamiento una vez levantado el dedo
    private bool isMoving;

    // para saber si se está moviendo
    public static bool move;

    [SerializeField] private float minZoom = 6f;
    [SerializeField] private float maxZoom = 8f;
    [SerializeField] private float zoomSpeed = 0.02f;

    [SerializeField] private float limitZForward, limitZBakcward;
    [SerializeField] private float limitXRight, limitXLeft;
    [SerializeField] private float limitYUp, limitYDown;
    
    

    float groundZ;
    public void SetWorldLimits(float limitWorldZForward, float limitWorldZBackward, float limitWorldXRight, float limitWorldXLeft, float limitWorldYUp, float limitWorldYDown)
    {
        limitZForward = limitWorldZForward;
        limitZBakcward = limitWorldZBackward;
        limitXRight = limitWorldXRight;
        limitXLeft = limitWorldXLeft;
        limitYUp = limitWorldYUp;
        limitYDown = limitWorldYDown;
    }
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {/*
        if (Input.GetMouseButtonDown(0))
        {
            startTouch = GetWorldPosition(groundZ);
            previousPosition = startTouch;

            
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(velocity > 0)
            {
                isMoving = true;

                if (velocity >= 20)
                {
                    velocity = 20;
                }
            }
        }

        
        if (isMoving)
        {
            
            cam.transform.position += direction * velocity * Time.deltaTime;
            velocity -= Time.deltaTime * brakeForce;
        }
        
        if(velocity <= 0)
        {
            isMoving = false;
        }
        */
       

    }

    void LateUpdate()
    {
        // Si tocamos la pantalla con 2 dedos
        if (Input.touchCount == 2)
        {
            // Guardamos la posición de los dedos
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Guardamos la posición anterior de los toques
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Guardamos la magnitud de los toques al principio, que lo que hace aquí es guardar la distancia entre ellos.
            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;

            // Guardamos la distancia actual de los dedos
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            // Guardamos la diferencia de ambas distancias para saber si estamos ampliando o disminuyendo
            float difference = prevMagnitude - currentMagnitude;

            // Realizamos el zoom
            Zoom(difference * zoomSpeed);
        }
        if (Input.GetMouseButtonDown(0))
        {
            startTouch = cam.ScreenToWorldPoint(Input.mousePosition);//GetWorldPosition(groundZ);
            previousPosition = startTouch;


        }
#if UNITY_EDITOR

        // Si no tenemos 2 dedos tocando la pantalla, nos movemos
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {

            Vector3 newPosition = cam.ScreenToWorldPoint(Input.mousePosition);//GetWorldPosition(groundZ);

            direction = startTouch - newPosition;

            cam.transform.position += direction * speed * Time.deltaTime;
            velocity = (newPosition - previousPosition).magnitude / Time.deltaTime;

            if (startTouch != newPosition)
            {
                move = true;
            }

            previousPosition = newPosition;




        }
        else
        {
            if (velocity > 0)
            {
                isMoving = true;

                if (velocity >= minVelocity)
                {
                    velocity = minVelocity;
                }
            }
            if (velocity <= 0)
            {
                isMoving = false;
                move = false;
            }
            if (isMoving)
            {

                cam.transform.position += direction * velocity * Time.deltaTime;
                velocity -= Time.deltaTime * brakeForce;
            }
        }


        cam.transform.position = new Vector3(Mathf.Clamp(cam.transform.position.x, limitXLeft, limitXRight),
                                             62f,
                                             Mathf.Clamp(cam.transform.position.z, limitZBakcward, limitZForward));

#elif UNITY_ANDROID

        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {

            Vector3 newPosition = cam.ScreenToWorldPoint(Input.mousePosition);//GetWorldPosition(groundZ);

            direction = startTouch - newPosition;

            cam.transform.position += direction * speed * Time.deltaTime;
            velocity = (newPosition - previousPosition).magnitude / Time.deltaTime;

            if (startTouch != newPosition)
            {
                move = true;
            }

            previousPosition = newPosition;




        }
        else
        {
            if (velocity > 0)
            {
                isMoving = true;

                if (velocity >= minVelocity)
                {
                    velocity = minVelocity;
                }
            }
            if (velocity <= 0)
            {
                isMoving = false;
                move = false;
            }
            if (isMoving)
            {

                cam.transform.position += direction * velocity * Time.deltaTime;
                velocity -= Time.deltaTime * brakeForce;
            }
        }


        cam.transform.position = new Vector3(Mathf.Clamp(cam.transform.position.x, limitXLeft, limitXRight),
                                             62f,
                                             Mathf.Clamp(cam.transform.position.z, limitZBakcward, limitZForward));


#endif


    }

    private Vector3 GetWorldPosition(float z)
    {
        Ray mousePosition = cam.ScreenPointToRay(Input.mousePosition);

        Plane ground = new Plane(Vector3.forward, new Vector3(0f, 0f, z));

        float distance;

        ground.Raycast(mousePosition, out distance);

        return mousePosition.GetPoint(distance);
    }

    private void Zoom(float increment)
    {
        // Limitamos el zoom de la cámara en el mínimo y máximo indicado
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize + increment, minZoom, maxZoom);
    }


}
