﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Namespace para poder usar DoTween
using DG.Tweening;

public class Ambulance : TypeInteraction
{
    // Estados de la ambulancia
    public enum State {OnWay, Parking, Parked, Exit}

    // Estado actual de la ambulancia
    [SerializeField] private State state;

    // Waypoints para aparcar y realizar la animación
    public Transform[] entryParkingWP, exitParkingWP;
    Vector3[] pathEntryParking, pathExitParking;

    // Indica cual es el waypoint de parking
    public int destiny;

    // Indica el sitio donde se dirigirá para aparcar
    public Transform waypoint;

    // Distancia al objetivo
    [SerializeField] private float distanceToTarget;

    // Velocidad de desplazamiento
    [SerializeField] private float maxSpeed, minSpeed;

    // Velocidad actual
    [SerializeField] private float speed;

    // Velocidad para decelerar
    [SerializeField] private float deceleration;

    // Para saber si nos dirigimos al aparcamiento o estamos saliendoy acelerar o decelerar
    [SerializeField]private bool toPark = true;

    Tween entryParking, exitParking;


    public override void ExecuteInteraction()
    {
        if (state == State.Parked)
        {
            StartCoroutine(ExitParking());
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = maxSpeed;

        // Creamos un array de vectores 3 con el tamaño de los waypoints y los rellenamos
        
        

        waypoint = ParkingManager.instance.Busy(destiny);

        state = State.OnWay;
    }

    // Update is called once per frame
    void Update()
    {
        // Si estamos estamos entrando, ejecutamos el método de entrada al parking, si no ejecutamos el de salida

            //GetInParking();    
        if(state == State.OnWay)
        {
            OnWay();
        }
        

    }

    /// <summary>
    /// Método para que la ambulancia aparque
    /// </summary>
    public void OnWay()
    {

        //transform.DOMove(parkingWaypoint.position, 2f);
        
        transform.position = Vector3.MoveTowards(transform.position, waypoint.position, speed * Time.deltaTime);
        
        if (Vector3.Distance(transform.position, ParkingManager.instance.entrySiteWP[destiny].position) <= distanceToTarget && toPark)
        {
            speed = Mathf.Lerp(speed, minSpeed, deceleration * Time.deltaTime);
             
        } else
        {
            speed = Mathf.Lerp(speed, maxSpeed, deceleration * Time.deltaTime / 6);

            if(Vector3.Distance(transform.position, waypoint.position) < 0.2f)
            {
                Destroy(gameObject);
            }
        }
            
        if(Vector3.Distance(transform.position, ParkingManager.instance.entrySiteWP[destiny].position) <= 0.2f && toPark){
            StartCoroutine(EntryParking());    
        }
        
            /*
            transform.position = Vector3.MoveTowards(transform.position, parkingWaypoints[destiny].position, minSpeed * Time.deltaTime);
            //children.transform.rotation = Quaternion.Slerp(children.transform.rotation, Quaternion.LookRotation(parkingWaypoints[destiny].position), 0.01f);
            if (Vector3.Distance(transform.position, parkingWaypoints[destiny].position) <= distanceToTarget)
            {
                //animator.SetTrigger("Entry");
                //children.transform.rotation = Quaternion.Lerp(children.transform.rotation, parkingWaypoints[destiny].rotation, 0.01f);
            }*/
               
    }

    public IEnumerator EntryParking()
    {
        state = State.Parking;
        int tempSizeEntryPath = waypoint.childCount;
        pathEntryParking = new Vector3[tempSizeEntryPath];

        for (int i = 0; i < pathEntryParking.Length; i++)
        {
            pathEntryParking[i] = waypoint.GetChild(i).position;
        }

        entryParking = transform.DOPath(pathEntryParking, 4f, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.1f);

        yield return entryParking.WaitForCompletion();

        
        state = State.Parked;
        waypoint = ParkingManager.instance.exitSiteWP[destiny];
    }

    /// <summary>
    /// Método para que la ambulancia salga del parking
    /// </summary>
    [ContextMenu("Salir parking")]
    public IEnumerator ExitParking()
    {
        // Indicamos que ya no estamos aparcados para que no se vuelva a ejecutar la corrutina si le volviera a dar
        state = State.Exit;
        toPark = false;
        Debug.Log("Ganamos dinero y ambulancia fuera!!");
        //Añadimos dinero al total del jugador -- ShowMoney() no será necesario cuando funcione el hud, es solo para mostrarlo en el inspector.
        GameData.currentData.money.AddMoney(new Money(Random.Range(100, 999)/*, ""*/));

        int tempSizeExitPath = waypoint.childCount;

        pathExitParking = new Vector3[tempSizeExitPath];

        for (int i = 0; i < pathExitParking.Length; i++)
        {
            pathExitParking[i] = waypoint.GetChild(i).position;
        }

        exitParking = transform.DOPath(pathExitParking, 2f, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.1f).SetEase(Ease.InSine);

        // Indicamos que ese hueco se queda libre
        ParkingManager.instance.isBusy[destiny] = false;

        // Preguntamos si el parking estaba lleno y si lo estaba, le decimos que ya no lo está
        ParkingManager.instance.ambulanceComplete = ParkingManager.instance.ambulanceComplete ? false : ParkingManager.instance.ambulanceComplete;

        waypoint = ParkingManager.instance.exitParking;
        

        // Incrementamos el destino para salir hacia el siguiente waypoint de la carretera.
        //destiny++;
        yield return exitParking.WaitForCompletion();
        
        // Nos movemos hacia el nuevo destino
        //transform.position = Vector3.MoveTowards(transform.position, ParkingManager.instance.parkingWaypoints[destiny].position, speed * Time.deltaTime);
        //transform.LookAt(waypoints[destiny].position);
        state = State.OnWay;
        
    }
    



}
