using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : InteractionAfterClick
{

    [Range(1, 5)]
    public int attendantsAmount;
    public int currentAttendants;
    public RoomNavigation navigation;
    public bool receptionDesk;
    public void Awake()
    {
        navigation = GetComponent<RoomNavigation>();
        canvas = GameObject.FindObjectOfType<RoomPanelController>().GetComponent<Canvas>();
        /*SpawnManager spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        if (spawnManager != null && !receptionDesk)
        {
            spawnManager.posibleDestinations.Add(this);
        }*/
    }

    public override void Update()
    {
        if (attendantsAmount != currentAttendants)
        {
            currentAttendants = attendantsAmount;
            navigation.EnableAttendants(currentAttendants);

        }


        base.Update();

    }


    public override void ExecuteFingerUp()
    {
        canvas.enabled = true;
        RoomPanelController roomMenu = canvas.GetComponent<RoomPanelController>();
        roomMenu.Initialize(GetComponent<RoomData>());
        /*startTemp = false;
        if(cont <= desiredTime && !MoveCamera.move)
        {
            
        }*/
    }
}
