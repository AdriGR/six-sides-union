﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateManager : MonoBehaviour
{
    // Prefab de la ambulancia a instanciar
    public GameObject ambulance;
    // Variable para instanciar la ambulancia
    [SerializeField] private float timingInstantiate = 5;
    // Contador para llevar la cuenta atras para la instanciación de ambulancias
    [SerializeField] private float cont;

    // Start is called before the first frame update
    void Start()
    {
        cont = timingInstantiate;
    }
    
    // Update is called once per frame
    void Update()
    {
        cont -= Time.deltaTime;

        if(cont <= 0)
        {
            InstantiateAmbulance();
            cont = timingInstantiate;
        }
    }
    /// <summary>
    /// Método para instanciar ambulancias
    /// </summary>
    private void InstantiateAmbulance()
    {
        // Si el parking está completo, no instanciamos ambulancia
        if (ParkingManager.instance.ambulanceComplete)
        {
            return;
        }

        // Variable temporal que indicara a la ambulancia donde ir y al parking que sitio ocupar
        int tempParking = ParkingManager.instance.IsFreeParking();

        // Instanciamos la ambulancia
        GameObject tempAmbulance = Instantiate(ambulance, transform.position, Quaternion.LookRotation(transform.forward));

        // Le indicamos a la ambulancia instanciada donde se tiene que dirigir
        tempAmbulance.GetComponent<Ambulance>().destiny = tempParking;
    }
}
