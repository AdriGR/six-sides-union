﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class InstantiateCar : MonoBehaviour
{
    // Objeto que instanciará
    public GameObject[] car;

    // Tiempo que tardará en instanciar otro vehículo
    public float delay;

    // Primer waypoint al que se dirigiran los vehículos al instanciarse
    public Transform firstWaypoint;

    // Objeto donde instanciaremos todos los coches
    public Transform cars;

    // Contador interno para realizar la cuenta atrás
    private float cont;

    //public UnityEvent evento;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("instanciado: " + gameObject.name);
        cont = delay;
    }

    // Update is called once per frame
    void Update()
    {
        cont -= Time.deltaTime;

        if(cont <= 0)
        {
            CreateCar();
            cont = delay;
        }
    }

    public void CreateCar()
    {
        if (CarParkingManager.instance.parkingComplete)
        {
            return;
        }
        // Creamos un número aleatorio para escoger que vehículo instanciar
        int randomcar = Random.Range(0, car.Length);
        // Creamos el objeto y decimos que salga mirando hacia el forward del instanciador
        GameObject tempCar = Instantiate(car[randomcar], transform.position, Quaternion.LookRotation(transform.forward));

        // Recuperamos el componente y le decimos cual será su primer waypoint
        tempCar.GetComponent<Car>().nextWaypoint = firstWaypoint;


    }
}
