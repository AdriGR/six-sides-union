﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoTweenTest : MonoBehaviour
{
    public Transform carModel;
    public Transform[] path;
    Vector3[] pathPositions;
    public float duration;
    Tween parkingTween;
    public bool inPosition;
    public ClientSpawner spawner;
    // Start is called before the first frame update
    void Start()
    {
        pathPositions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            pathPositions[i] = path[i].position;
        }


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!inPosition)
            {
                StartCoroutine(EnterPakingPosition());
            }
            else 
            {
                StartCoroutine(LeaveParkingPosition());
            }
        }
        
    }
    IEnumerator EnterPakingPosition()
    {
        
        parkingTween = carModel.DOPath(pathPositions, duration, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.2f).SetAutoKill(false);
        yield return parkingTween.WaitForCompletion();
        inPosition = true;
        spawner.SpawnClient();
            
    }
    IEnumerator LeaveParkingPosition()
    {
        parkingTween.PlayBackwards();        
        yield return parkingTween.WaitForCompletion();
        inPosition = false;
    }
}
