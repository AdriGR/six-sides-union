﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingManager : MonoBehaviour
{
    // Para saber si el sitio está ocupado o libre y su array de sitios
    public bool[] isBusy = new bool[4];
    public Transform[] entrySiteWP;
    public Transform[] exitSiteWP;
    public Transform exitParking;

    // Para saber si caben mas ambulancias
    public bool ambulanceComplete;

    public static ParkingManager instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = GetComponent<ParkingManager>();
        }
    }


    /// <summary>
    /// Método para saber cuál es la siguiente plaza disponible
    /// </summary>
    /// <returns></returns>
    [ContextMenu("Parking libre")]
    public int IsFreeParking()
    {
        if (ambulanceComplete)
        {
            return -1;
        }

        int free = 0;
        
        for(int i = 0; i < isBusy.Length; i++)
        {
            if (!isBusy[i])
            {
                free = i;
                break;
            } 
        }
        return free;        
    }

    /// <summary>
    /// Método para ocupar una plaza de parking de la ambulancia
    /// </summary>
    /// <param name="position"></param>
    public Transform Busy(int position)
    {
        // Ocupamos la posición indicada
        isBusy[position] = true;

        // Suponemos que el parking ya está lleno por defecto
        bool parkingAmbulanceFull = true;

        // Recorremos los huecos de la ambulancia y si detectamos alguno libre, indicamos que no esta lleno
        for(int i = 0; i < isBusy.Length; i++)
        {
            if (!isBusy[i])
            {
                parkingAmbulanceFull = false;
            }
        }

        // Si el parking está lleno, indicamos al mánager que indique que está lleno, en caso contrario no.
        ambulanceComplete = parkingAmbulanceFull ? true : false;

        // Decimos a la ambulancia que debe de dirigirse hacia ese waypoint
        return entrySiteWP[position];
    }
}
