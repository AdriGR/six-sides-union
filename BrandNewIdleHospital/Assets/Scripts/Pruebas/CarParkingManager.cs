﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase para la plaza de parking, con su índice, sitio, y si está o no ocupada
[System.Serializable]
public class ParkingLot {
    public int index;
    public bool isBusy;
    public Transform site;
}

public class CarParkingManager : MonoBehaviour
{
    // Para hacer comprobaciones de cuando está el parking lleno
    public int full = 1;

    // Tamaño máximo del parking
    private int maxParking = 138;

    // Capacidad actual del parking
    public int capacity = 6;

    [Header("Upgrades")]
    // número de aparcamientos que se incrementarán en cada upgrade
    public int incrementParking = 6;

    // Array con los distintos upgrades del parking
    public GameObject[] upgrades;

    // Indicador del nivel actual para saber a que waypoint referenciar para el camino
    public int level = 1;

    // Para saber si el parking está completo
    public bool parkingComplete;

    // Para saber si hemos llegado al nivel máximo de upgrade donde ya no podemos ampliar mas, a partir de que esté activada, solo aumentarán sitios
    public bool isUnderground;

    // Indicador para indicar a partir de qué numero de sitios direccionamos al subterráneo
    public int underground = 3;

    // Referencia al primer waypoint del parking
    public Transform firstWaypoint;

    // Referencia al ultimo wp antes de empezar los wp del parking
    public Waypoint startWPParking;

    // Referencia al waypoint a coger una vez vaya a salir del parking
    public Transform waypointToExit;

    public List<Car> carsInGame = new List<Car>();

    public static CarParkingManager instance;

    // Lista con las plazas de parkings
    public List<ParkingLot> sites = new List<ParkingLot>();
    public List<ParkingLot> oldSites = new List<ParkingLot>();
    public List<Checker> checkers = new List<Checker>();

    ParkingLot parkingLot;

    public GameObject currentParking;
    private void Awake()
    {
        if(instance == null)
        {
            instance = GetComponent<CarParkingManager>();
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        GameData.currentData.ChangeParkingLevel(level);
    }
    private void Update()
    {
        CheckFree();
    }


    /// <summary>
    /// Método que se llamara cuando inice el juego
    /// </summary>
    public void Init()
    {
        waypointToExit = GameObject.FindGameObjectWithTag("ExitWaypoint").transform;
        firstWaypoint = PathParking(currentParking.transform);
        startWPParking.nextWaypoint = firstWaypoint;
        AddParkingSites(currentParking);
        AddCheckers(currentParking);
    }

    /// <summary>
    /// Método para añadir los sitios de aparcamiento
    /// </summary>
    public void AddParkingSites(GameObject currentP)
    {
        // Si tenemos sitios guardados en la lista, la limpiamos.
        if (sites.Count != 0)
        {
            sites.Clear();
        }

        Transform tempParkingLot;
        tempParkingLot = currentP.transform.Find("NavigationParking").transform.Find("ParkingLots");
        Debug.Log(tempParkingLot.parent.parent.name);
        int PLotChildren = tempParkingLot.childCount;

        
        

        // Rellenamos la lista de plazas del aparcamiento
        for (int i = 1; i <= PLotChildren; i++)
        {
            parkingLot = new ParkingLot();
            parkingLot.index = i;
            parkingLot.isBusy = false;
            // Si número de parking es mayor que el número fijado para el subterraneo y hemos alcanzado el subterráneo, guardamos en dicho sitio la posición subterránea
            if (parkingLot.index > underground) //&& isUnderground
            {
                parkingLot.site = tempParkingLot.GetChild(tempParkingLot.childCount - 1);
            } else
            // De lo contrario guardamos el sitio correspondiente
            {
                parkingLot.site = tempParkingLot.GetChild(i - 1);
            }


            sites.Add(parkingLot);
        }
    }

    /// <summary>
    /// Método para rellenar la lista de los checkers
    /// </summary>
    public void AddCheckers(GameObject currentP)
    {
        Transform tempChecker;
        tempChecker = currentP.transform.Find("NavigationParking").transform.Find("Checkers");
        int checkersChildren = tempChecker.childCount;

        for(int i = 0; i < checkersChildren; i++)
        {            
            checkers.Add(tempChecker.GetChild(i).GetComponent<Checker>());
        }
    }

    /// <summary>
    /// Método para dejar un sitio libre en el parking
    /// </summary>
    /// <param name="index"></param>
    public void FreeSite(int index)
    {
        sites[index - 1].isBusy = false;
    }

    /// <summary>
    /// Método para comprobar si el parking está lleno
    /// </summary>
    void CheckFree()
    {
        parkingComplete = true;
        for (int i = 0; i < sites.Count; i++)
        {
            parkingComplete = parkingComplete && sites[i].isBusy;
            
        }
    }
    /// <summary>
    /// Método para buscar un hueco libre en el parking donde el vehículo posteriormente cogerá su aparcamiento.
    /// </summary>
    /// <returns></returns>
    [ContextMenu("Prueba parking libre")]
    public int SearchParking()
    {
        // Creamos una lista temporal para almacenar los sitios libres
        List<ParkingLot> tempSite = new List<ParkingLot>();
        
        // Limpiamos la lista para las siguientes búsquedas no repetir
        tempSite.Clear();

        // Recorremos todos los huecos y añadimos a la lista temporal los que estén libres
        foreach (ParkingLot site in sites)
        {
            if (!site.isBusy)
            {
                tempSite.Add(site);
            } 
        }

        // Generamos un sitio aleatorio libre para asignárselo al vehículo
        int index = Random.Range(0, tempSite.Count);

        // Ocupamos esa plaza que hemos asignado para no volverla a seleccionar mientras esté ocupada
        foreach (ParkingLot site in sites)
        {
            if(site.index == tempSite[index].index)
            {
                site.isBusy = true;
                break;
            }
        }

        // Devolvemos su número de plaza
        return tempSite[index].index;     
    }

    /// <summary>
    /// Método para buscar parking que será llamado desde checker para devolver el parking en el caso de que coincidiera con el que busca el vehículo
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public Transform FindParking(int index)
    {
        // Creamos un temporal para almacenar la plaza de aparcamiento que se asignará
        ParkingLot tempParking = null;
        
        // Buscamos el aparcamiento indicado por el index
        foreach (ParkingLot site in sites)
        {
            if(site.index == index)
            {
                tempParking = site;
                break;
            }
        }

        // Devolvemos el lugar que ocupa ese aparcamiento
        return tempParking.site;
    }

    /// <summary>
    /// Método para localizar el primer waypoint del parking
    /// </summary>
    /// <returns></returns>
    public Transform PathParking(Transform currentParkingTransform)
    {
        Debug.Log(currentParkingTransform);
        Transform waypointPath = currentParkingTransform.Find("NavigationParking").transform.Find("Waypoints").GetChild(0);
        return waypointPath;   
    }
    
    /// <summary>
    /// Método para destruir los coches que no estén aparcados al upgradear el parking
    /// </summary>
    private void DestroyCars()
    {
        // Creamos una lista temporal para almacenar los coches aparcados y luego pasarlos a la lista de coches en juego
        List<Car> tempListCar = new List<Car>();

        // Recorremos la lista de coches que hay en el juego
        foreach (Car car in carsInGame)
        {
            // Si no están aparcados, dejamos ese sitio libre y destruimos el coche, de lo contrario almacenamos ese coche en la lista temporal
            if (!car.isParked)
            {
                FreeSite(car.destiny);
                Destroy(car.gameObject);
            } else
            {
                tempListCar.Add(car);
            } 
        }

        // Limpiamos la lista de coches en juego y la igualamos a la temporal para que contenga solo los coches aparcados
        carsInGame.Clear();
        carsInGame = tempListCar;
    }

    /// <summary>
    /// Método para buscar las referencias perdidas de los coches aparcados
    /// </summary>
    /// <param name="car"></param>
    public void FindMissingReferences(Car car)
    {
        for(int i = 0; i < checkers.Count; i++)
        {
            for(int j = 0; j < checkers[i].sites.Length; j++)
            {
                if(checkers[i].sites[j] == car.destiny)
                {
                    car.exitSite = checkers[i].exit;
                    break;
                }
            }
        }

        car.controlPanelParking = currentParking.GetComponentInChildren<ControlPanelParking>();
    }

    /// <summary>
    /// Método para mover los coches aparcados a su sitio correspondiente al upgradear el parking
    /// </summary>
    /// <param name="car"></param>
    public void MoveCars(Car car)
    {
        
        // Recorremos la lista antigua y guardamos si esos sitios estaban ocupados o no
        for(int i = 0; i < oldSites.Count; i++)
        {
            
            if (car.parkingWaypoint == oldSites[i].site)
            {
                sites[i].isBusy = oldSites[i].isBusy;
                car.parkingWaypoint = sites[i].site;
                int destination = sites[i].site.childCount;
                car.transform.position = sites[i].site.GetChild(destination - 1).position;
                //car.Reset();
                Debug.Log("Coche emparentado a: " + sites[i].site);
            }
        }

        
    }

    /// <summary>
    /// Método para aumentar el parking
    /// </summary>
    [ContextMenu("Upgrade")]
    public void Upgrade()
    {
        if(level == 2)
        {
            return;
        }
        // Si no hemos llegado al nivel del subterraneo, llamamos al método que recoge el camino a seguir de los waypoints de dicho upgrade
        if (!isUnderground)
        {
            firstWaypoint = PathParking(currentParking.transform);
        }

        // Guardamos la lista para luego poder guardar los sitios ocupados
        foreach(ParkingLot park in sites)
        {
            oldSites.Add(park);
        }

        // Destruimos los coches que no estén aparcados        
        DestroyCars();
        
        // Incrementamos el máximo del parking en el incremento establecido y sumamos 1 nivel
        capacity += incrementParking;
        level++;

        // Llamamos al método para aumentar el parking
        GameData.currentData.ChangeParkingLevel(level);

        // Actualizamos el indicador del parking
        currentParking.GetComponentInChildren<ControlPanelParking>().UpdateIndicator();
    }

    
}
