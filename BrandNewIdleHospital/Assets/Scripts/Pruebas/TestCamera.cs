﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCamera : MonoBehaviour {

    [SerializeField] private float speed;

    // Update is called once per frame
    void LateUpdate()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0f);
            /*
            transform.position = new Vector3(
               Mathf.Clamp(transform.position.x, -64f, 23.5f),
               Mathf.Clamp(transform.position.y, 60f, 62f),
               Mathf.Clamp(transform.position.z, -13f, 180f)
                );*/
        }
    }
   
}
