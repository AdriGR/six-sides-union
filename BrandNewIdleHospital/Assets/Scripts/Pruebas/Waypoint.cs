﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{

    // Siguiente waypoint al que se dirigirá el vehículo
    public Transform nextWaypoint;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.CompareTag("StartWPParking"))
        {
            //nextWaypoint = CarParkingManager.instance.firstWaypoint;
            CarParkingManager.instance.startWPParking = this;
        } else if (gameObject.CompareTag("FinishWPParking"))
        {
            nextWaypoint = CarParkingManager.instance.waypointToExit;
        }
    }

    /// <summary>
    /// Mëtodo que será llamado desde el coche para saber donde tiene que aparcar
    /// </summary>
    /// <param name="siteCar"></param>
    public Transform DecisionToPark(Transform siteCar)
    {
       return nextWaypoint = siteCar.GetChild(0);
        
    }

    


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Car"))
        {
            // Recuperamos el componente y le pasamos el siguiente destino
            Car tempCar = other.GetComponent<Car>();
            if (nextWaypoint != null)
            {
                tempCar.nextWaypoint = nextWaypoint;
            }
            else 
            {
                Destroy(tempCar.gameObject);
            }
            
        }
    }
}
