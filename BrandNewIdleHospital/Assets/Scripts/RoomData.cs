using System;
using System.Collections.Generic;
using UnityEngine;


public class RoomData : MonoBehaviour
{
    public string roomName;
    public Sprite roomImage;
    RoomNavigation navigation;
    public Money buildingPrice;
    [Header("Level")]
    [Range(1, 3000)]
    public int desiredLevel = 1;
    public int currentLevel;
    public Money baseNextLevelCost;
    public Money currentNextLevelCost;
    bool updatingLevel;
    [Header("Incomes")]
    public Money baseIncome;
    public Money currentIncome;

    [Header("Attendants Amount")]
    [Range(1, 5)]
    public int desiredAttendants = 1;
    public int currentAttendants;
    public Money baseAttendantsAmountUpdate;
    public Money currentAttendantsAmountUpdate;

    [Header("Attendants Speed")]
    [Range(1, 100)]
    public int desiredSpeedLevel = 1;
    public int speedLevel;
    public float baseAttendSpeed;
    public float speedIncrementStep;
    public float currentAttendSpeed;
    public Money baseAttendSpeedUpdateCost;
    public Money currentNextAttendSpeedUpdateCost;

    [Header("LevelPrefabs")]
    [Range(1, 3)]
    public int desiredModelLevel;
    public int currentModelLevel;
    public GameObject[] levelPrefabs;
    public GameObject currentLevelRoom;
    private void Awake()
    {
        navigation = GetComponent<RoomNavigation>();
    }

    private void Start()
    {
        if (currentLevel == 0)
        {
            currentLevel = 1;
        }
        if (currentModelLevel == 0)
        {
            desiredModelLevel = 1;
        }
        //Initialize all 'Money' variables
        baseNextLevelCost = new Money(baseNextLevelCost.moneyAmount);
        currentNextLevelCost = new Money(baseNextLevelCost.moneyAmount);

        buildingPrice = new Money(buildingPrice.moneyAmount);
        baseIncome = new Money(baseIncome.moneyAmount);
        currentIncome = CalculateCurrentIncome(currentLevel, baseIncome);
        
        SetSpeed();

        baseAttendantsAmountUpdate = new Money(baseAttendantsAmountUpdate.moneyAmount);
        currentAttendantsAmountUpdate = new Money(baseAttendantsAmountUpdate.moneyAmount);
        baseAttendSpeedUpdateCost = new Money(baseAttendSpeedUpdateCost.moneyAmount);
        currentNextAttendSpeedUpdateCost = new Money(baseAttendSpeedUpdateCost.moneyAmount);
        currentNextAttendSpeedUpdateCost = CalculateNextSpeedCost(speedLevel, currentNextAttendSpeedUpdateCost);
       
    }
    Money CalculateNextLevelCost(int currentLevel, Money BaseCost)
    {
        //(Nivel de mejora * precio actual) ^ 2
        //costBase*(growthRate^owned)
        double tempMoney = Math.Pow(1.1, currentLevel);
        tempMoney *= BaseCost.moneyAmount;
        Money result = new Money(tempMoney);
        return result;
    }
    Money CalculateCurrentIncome(int level, Money baseMoney)
    {
        //(nivelActual*ingresoBase) ^ 1.2 
        //(productionBase*owned)*multipliers
        Money tempMoney = new Money();
        double a = baseMoney.moneyAmount * level + 1;
        tempMoney.moneyAmount = a * 1.2;
        Money result = new Money(tempMoney.moneyAmount);
        return result;
    }
    float CalculateCurrentSpeed(int speedLevel, float baseSpeed, float increment)
    {
        float result = 0.0f;
        result = baseSpeed + (speedLevel * increment);
        return result;
    }

    void SetSpeed()
    {
        currentAttendSpeed = CalculateCurrentSpeed(speedLevel, baseAttendSpeed, speedIncrementStep);
        for (int i = 0; i < navigation.attendants.Length; i++)
        {
            navigation.attendants[i].speed = currentAttendSpeed;
        }
        currentNextAttendSpeedUpdateCost = CalculateNextSpeedCost(speedLevel, currentNextAttendSpeedUpdateCost);
    }
    public void SpeedLevelUp()
    {
        GameData.currentData.money.SubstractMoney(currentNextAttendSpeedUpdateCost);
        speedLevel += 1;
        SetSpeed();
    }
    Money CalculateNextSpeedCost(int currentLevel, Money currentCost)
    {
        //Precio Actual ^ 1.1
        Money tempMoney = new Money();
        if (currentLevel > 1)
        {
            tempMoney.moneyAmount = currentCost.moneyAmount * 1.1;
        }
        else
        {
            tempMoney.moneyAmount = currentCost.moneyAmount;

        }

        Money result = new Money(tempMoney.moneyAmount);
        return result;
    }
    Money CalculateNextAttendantCost(int attendantsNumber, Money currentCost)
    {
        //(PrecioActual * attendantsAmount) ^ 1.1;
        double tempMoney = 0;
        if (attendantsNumber > 1)
        {
            double a = currentCost.moneyAmount * attendantsNumber;
            tempMoney = Math.Pow(a, 1.1);

        }
        else
        {
            tempMoney = currentCost.moneyAmount;

        }

        Money result = new Money(tempMoney);
        return result;
    }
    public void UpdateLevelTo(int level)
    {
        
        GameData.currentData.money.SubstractMoney(currentNextLevelCost);
        currentLevel = level;
        desiredLevel = level;
        currentIncome = CalculateCurrentIncome(level, baseIncome);
        currentNextLevelCost = CalculateNextLevelCost(level, baseNextLevelCost);

    }
    private void Update()
    {
        if (desiredLevel > currentLevel)
        {
            UpdateLevelTo(currentLevel + 1);
        }
        if (desiredSpeedLevel > speedLevel)
        {
            speedLevel = desiredSpeedLevel;
            
            SetSpeed();

            
        }
        //if (desiredAttendants > currentAttendants)
        //{
        //    AddAttendant();

        //}
        if (desiredModelLevel != currentModelLevel)
        {
            currentModelLevel = desiredModelLevel;
            SetLevelModel(currentModelLevel);
        }

    }
    public void AddAttendant()
    {
        GameData.currentData.money.SubstractMoney(currentAttendantsAmountUpdate);
        currentAttendants += 1;
        navigation.EnableAttendants(currentAttendants);
        currentAttendantsAmountUpdate = CalculateNextAttendantCost(currentAttendants, currentAttendantsAmountUpdate);
    }
    void SetLevelModel(int level)
    {
        if (currentLevelRoom != null)
        {
            Destroy(currentLevelRoom);
        }
        int levelToIndex = level - 1;
        for (int i = 0; i < levelPrefabs.Length; i++)
        {
            if (i == levelToIndex)
            {
                currentLevelRoom = GameObject.Instantiate(levelPrefabs[i], transform.Find("RoomSpawn"));

            }
        }
    }


  
   
   
   
   
   
}

