﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SphereCollider))]
public class DecisionPoint : MonoBehaviour
{
    public List<PathControl> terminusTo;
    public Address address;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Client>())
        {
            Client currentClient = other.GetComponent<Client>();
            if (currentClient.destinationAddress.level != address.level)
            {
                
                if (!CheckLevel(currentClient))
                {
                    CheckSector(currentClient);
                }
            }
            else if (currentClient.destinationAddress.sector != address.sector)
            {
                if (!CheckSector(currentClient))
                {
                    CheckPoint(currentClient);
                }
            }            
            else if (currentClient.destinationAddress.spot != address.spot)
            {
                CheckPoint(currentClient);
            }
            if (currentClient.attended && address == currentClient.entrance.address)
            {
                currentClient.SetTarget(currentClient.mySpawner.transform.position);
            }
            

        }
    }
    bool CheckLevel(Client currentClient)
    {
        bool result = false;
        if (currentClient.destinationAddress.level > address.level)
        {
            //Debug.Log("currentClient.destinationAddress.level > address.level " + gameObject.name + " DecisionPoint");
            foreach (PathControl path in terminusTo)
            {
                if (path.terminusB.address.level > address.level && path.terminusB.address.level <= currentClient.destinationAddress.level)
                {
                    //Debug.Log("path.terminusB.address.level > address.level && path.terminusB.address.level <= currentClient.destinationAddress.level " + gameObject.name + " DecisionPoint");
                    currentClient.currentPath = path.transform;
                    currentClient.currentWaypoint = path.waypointsPath[0];
                    currentClient.returning = false;
                    result = true;
                }
            }
        }
        else if (currentClient.destinationAddress.level < address.level)
        {
            //Debug.Log("currentClient.destinationAddress.level < address.level " + gameObject.name + " DecisionPoint");
            foreach (PathControl path in terminusTo)
            {
                if (path.terminusA.address.level < address.level && path.terminusA.address.level >= currentClient.destinationAddress.level)
                {
                    //Debug.Log("path.terminusB.address.level > address.level && path.terminusB.address.level <= currentClient.destinationAddress.level " + gameObject.name + " DecisionPoint");
                    currentClient.currentPath = path.transform;
                    currentClient.currentWaypoint = path.waypointsPath[path.waypointsPath.Length - 1];
                    currentClient.returning = true;
                    result = true;
                }
            }
        }
        return result;
    }
    bool CheckSector(Client currentClient)
    {
        bool result = false;
        if (currentClient.destinationAddress.sector > address.sector)
        {
            //Debug.Log("currentClient.destinationAddress.sector > address.sector " + gameObject.name + " DecisionPoint");
            foreach (PathControl path in terminusTo)
            {
                if (path.terminusB.address.sector > address.sector)
                {
                    currentClient.currentPath = path.transform;
                    currentClient.currentWaypoint = path.waypointsPath[0];
                    currentClient.returning = false;
                    result = true;
                }
            }
        }
        else if (currentClient.destinationAddress.sector < address.sector)
        {
            //Debug.Log("currentClient.destinationAddress.sector < address.sector " + gameObject.name + " DecisionPoint");
            foreach (PathControl path in terminusTo)
            {
                if (path.terminusA.address.sector < address.sector)
                {
                    currentClient.currentPath = path.transform;
                    currentClient.currentWaypoint = path.waypointsPath[path.waypointsPath.Length - 1];
                    currentClient.returning = true;
                    result = true;
                }
            }
        }
        return result;
    }
    bool CheckPoint(Client currentClient)
    {
        bool result = false;
        foreach (PathControl path in terminusTo)
        {
            if (path.terminusB.address.spot == currentClient.destinationAddress.spot)
            {
                currentClient.currentPath = path.transform;
                currentClient.currentWaypoint = path.waypointsPath[0];
                currentClient.returning = false;
                currentClient.attended = false;
                result = true;
            }
            else if (path.terminusA.address.spot == currentClient.destinationAddress.spot)
            {
                currentClient.currentPath = path.transform;
                currentClient.currentWaypoint = path.waypointsPath[path.waypointsPath.Length - 1];
                currentClient.returning = true;
                currentClient.attended = false;
                result = true;
            }

        }
        return result;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetComponent<SphereCollider>().radius);
    }
}
