using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomNavigation : MonoBehaviour
{
    RoomController myDeskControl;
    public BaseAttendant[] attendants;
    public Transform entryPoint;
    public Transform exitPoint;
    public LinePosition[] myLinePositions;
    public List<Client> clientsInLine;
    public List<Client> waitingClients;
    public Transform waitPoint;
    public float waitingAreaRadius;
    [Range(10,25)]
    public float waitingMove;
    float waitingMoveTimer;
    public Transform pathFromEntrance;
    public Address myAddress;
    public Address nextAddress;
    // Start is called before the first frame update
    void Start()
    {
        myDeskControl = GetComponent<RoomController>();
        myLinePositions = transform.GetComponentsInChildren<LinePosition>();
        waitingAreaRadius = waitPoint.GetComponent<SphereCollider>().radius;
    }

    // Update is called once per frame
    void Update()
    {
        CheckFreeAttendants();
        
        if (waitingMoveTimer > 0)
        {
            waitingMoveTimer -= Time.deltaTime;
        }
        else 
        {
            waitingMoveTimer = UnityEngine.Random.Range(waitingMove-5, waitingMove+5);
            WaitingRandomMove();
        }
    }
    void WaitingRandomMove()
    {
        if (waitingClients.Count > 0)
        {
            Vector2 waitPosition = UnityEngine.Random.insideUnitCircle * waitingAreaRadius;
            waitingClients[UnityEngine.Random.Range(0, waitingClients.Count)].SetTarget(new Vector3(waitPoint.position.x + waitPosition.x, waitPoint.position.y, waitPoint.position.z + waitPosition.y));
            
        }
    }
    public void EnableAttendants(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            if (!attendants[i].gameObject.activeSelf)
            {
                attendants[i].gameObject.SetActive(true);
            }
        }
    }
    void CheckFreeAttendants()
    {
        for (int i = 0; i < attendants.Length; i++)
        {
            if (attendants[i].gameObject.activeSelf && attendants[i].currentlyAttending == null)
            {
                if (clientsInLine.Count > 0)
                {
                    NextClient(attendants[i]);
                }

            }
        }
    }
    void NextClient(BaseAttendant attendant)
    {
        attendant.currentlyAttending = GetNextInLine();

    }
    Client GetNextInLine()
    {
        Client next = null;
        
        if (clientsInLine.Count > 0)
        {
            next = clientsInLine[0];

            clientsInLine.RemoveAt(0);

            ReassingLinePositions();
            return next;

        }
        
        return next;
    }
    void ReassingLinePositions()
    {
        if (waitingClients.Count > 0)
        {
            waitingClients[0].waiting = false;
            waitingClients[0].SetTarget(myLinePositions[myLinePositions.Length - 1].transform.position);
            clientsInLine.Add(waitingClients[0]);
            waitingClients.RemoveAt(0);

        }
        for (int i = 0; i < clientsInLine.Count; i++)
        {
            clientsInLine[i].SetTarget(myLinePositions[i].transform.position);
            myLinePositions[i].currentClient = clientsInLine[i];
        }
    }
}
[Serializable]
public class Address
{
    public int level;
    public int sector;
    public int spot;
    public Address(int addressLevel, int addressSector, int addressSpot)
    {
        level = addressLevel;
        sector = addressSector;
        spot = addressSpot;
    }
    public Address()
    {
        level = 0;
        sector = 0;
        spot = 0;
    }
}