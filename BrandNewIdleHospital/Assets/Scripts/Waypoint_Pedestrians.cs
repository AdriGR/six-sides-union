﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint_Pedestrians : MonoBehaviour
{

    public Waypoint_Pedestrians previousWaypoint;
    public Waypoint_Pedestrians nextWaypoint;

    [Range(0.0f, 5.0f)]
    public float width = 1.0f;
    public List<Waypoint_Pedestrians> branches;
    Vector3 GetPosition()
    {
        Vector3 maxBound = transform.position + transform.right * width / 2;
        Vector3 minBound = transform.position - transform.right * width / 2;
        return Vector3.Lerp(maxBound, minBound, Random.Range(0.0f, 0.1f));
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    Client pedestrian = null;
    //    if (other.GetComponent<Client>() != null)
    //    {
    //        pedestrian = other.GetComponent<Client>();
    //        if (!pedestrian.attended)
    //        {
    //            pedestrian.AddTravelledPath(this);
    //        }
            
    //    }
    //    if (pedestrian != null)
    //    {
    //        if (!pedestrian.returning)
    //        {
    //            if (nextWaypoint != null)
    //            {
    //                pedestrian.SetTarget(nextWaypoint.transform.position);
    //            }
    //            else
    //            {
    //                Debug.Log("TargetReached");
    //            }
    //        }
    //        else 
    //        {
    //            if (previousWaypoint != null)
    //            {
    //                pedestrian.SetTarget(previousWaypoint.transform.position);
    //            }
    //            else
    //            {
    //                if (pedestrian.attended)
    //                {
    //                    pedestrian.SetTarget(pedestrian.mySpawner.transform.position);
    //                }
    //                Debug.Log("TargetReached");
    //            }
    //        }
            
    //    }
    //}
}
