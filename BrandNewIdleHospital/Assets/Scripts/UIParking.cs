using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

public class UIParking : MonoBehaviour
{
    [Header("Information")]
    public TextMeshProUGUI levelParkingTXT;
    public TextMeshProUGUI sitesTXT;
    public TextMeshProUGUI ratioClientsTXT;
    public TextMeshProUGUI usagePercentageTXT;

    [Header("UpdateParking")]
    public Button UpdateBTN;
    public TextMeshProUGUI moneyUpdateTXT;

    [Header("Publish")]
    public Button publishBTN;
    public TextMeshProUGUI moneyPublishTXT;
    public TextMeshProUGUI levelPublishTXT;

    


    // Start is called before the first frame update
    void Start()
    {
        //updateParking += CarParkingManager.instance.Upgrade;
        UpdateBTN.onClick.AddListener(CarParkingManager.instance.Upgrade);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
