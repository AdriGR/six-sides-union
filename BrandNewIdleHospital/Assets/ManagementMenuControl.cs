﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ManagementMenuControl : MonoBehaviour
{
    public ManagementOffice myOffice;
    public GameObject[] roomBoxes;
    public  void Initialize()
    {
        UpdateRooms(GameData.currentData.buildRooms);
    }
    void UpdateRooms(int currentRoom)
    {
        for (int i = 0; i < roomBoxes.Length; i++)
        {
            if (i <= currentRoom)
            {
                roomBoxes[i].transform.Find("RoomNameTXT").GetComponent<TMP_Text>().text = GameData.currentData.allRoomsNames[i];
                roomBoxes[i].transform.Find("Text (TMP)").GetComponent<TMP_Text>().text = "Esta sala ya la tienes construida";
                roomBoxes[i].transform.Find("UpgradeBTN").gameObject.SetActive(false);
                roomBoxes[i].transform.Find("CheckImage").gameObject.SetActive(true);
               


            }
            else if (i == currentRoom + 1)
            {
                roomBoxes[i].transform.Find("Text (TMP)").GetComponent<TMP_Text>().text = "Esta es la que toca";
                roomBoxes[i].transform.Find("RoomNameTXT").GetComponent<TMP_Text>().text = GameData.currentData.allRoomsNames[i];
                roomBoxes[i].transform.Find("UpgradeBTN").Find("MoneyIMG").Find("MoneyTXT").GetComponent<TMP_Text>().text = GameData.currentData.allRoomsCosts[i].shortValue;
                roomBoxes[i].transform.Find("UpgradeBTN").GetComponent<Button>().interactable = true;
                roomBoxes[i].transform.Find("UpgradeBTN").GetComponent<Image>().color = Color.green;
            }
            else
            {
                roomBoxes[i].transform.Find("RoomNameTXT").GetComponent<TMP_Text>().text = GameData.currentData.allRoomsNames[i];
                roomBoxes[i].transform.Find("Text (TMP)").GetComponent<TMP_Text>().text = "Tienes que construir la sala anterior";
                roomBoxes[i].transform.Find("UpgradeBTN").Find("MoneyIMG").Find("MoneyTXT").GetComponent<TMP_Text>().text = GameData.currentData.allRoomsCosts[i].shortValue;
                roomBoxes[i].transform.Find("UpgradeBTN").GetComponent<Button>().interactable = false;
                roomBoxes[i].transform.Find("UpgradeBTN").GetComponent<Image>().color = Color.grey;
            }
        }
        
    }
    public void CloseMenu()
    {
        GetComponent<Canvas>().enabled = false;
    }
    public void BuildARoom()
    {
        if (GameData.currentData.buildRooms >= 1) { return; }//LIMITADOR: solo permite contruir una sala
        
        if (GameData.currentData.money.IsEnough(GameData.currentData.allRoomsCosts[GameData.currentData.buildRooms + 1]))
        {
            GameData.currentData.money.SubstractMoney(GameData.currentData.allRoomsCosts[GameData.currentData.buildRooms + 1]);
            GameData.currentData.buildRooms += 1;
            GameData.currentData.ChangeHospitalLevel(GameData.currentData.buildRooms+1);
            UpdateRooms(GameData.currentData.buildRooms);
        }
        
    }
    
}
