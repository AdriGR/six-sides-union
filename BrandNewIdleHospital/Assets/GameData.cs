using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Numerics;

public class GameData : MonoBehaviour
{
    
    public static GameData currentData;
    //public string totalMoney;
    [Header("Money")]
    public Money money = new Money();
    public int currentDiamonds;
    public Money overallMoneyForMinute;
    //public Money moneyAdded;
    //public Money moneySubstract;
    //public bool iCanBuy;
    //public bool buy;
    [Header("City")]
    [Range(1, 2)]
    public int desiredCityLevel;
    public int currentCityLevel;
    public GameObject currentCityGO;
    public GameObject[] levelCityGO;
    [Header("Hospital")]
    [Range(1, 2)]
    public int desiredHospitalLevel;
    public int currentHospitalLevel;
    public GameObject currentHospitalGO;
    public GameObject[] levelHospitalGO;
    [Header("Parking")]
    [Range(1, 2)]
    public int desiredParkingLevel;
    public int currentParkingLevel;
    public GameObject currentParkingGO;
    public GameObject[] levelParkingGO;
    [Header("Reception")]
   
    public Transform receptionSpawnPoint;
    public GameObject receptionRoom;
    GameObject receptionInstance;
    [Header("Rooms")]
    public int buildRooms;
    public Transform[] hospitalRoomsSpawnPoints;
    public GameObject[] hospitalRoomsPrefabs;
    public string[] allRoomsNames;
    public Money[] allRoomsCosts;
    Camera cam;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (currentData == null)
        {
            currentData = this;
        }
        else 
        {
            Destroy(gameObject);
        }
        // Hay que recoger los datos guardados y asignarlos al dinero que tenga el jugador
        // y el nivel actual de la ciudad, el hospital y el parking.
        // Como a�n no hay nada de eso, comienza en el nivel 1
        desiredCityLevel = 1;
        desiredHospitalLevel = 1;
        desiredParkingLevel = 1;
        cam = GameObject.Instantiate(Resources.Load("Main Camera")) as Camera;
        //moneyAdded = new Money(moneyAdded.moneyAmount, moneyAdded.moneyPeriod);
        //moneySubstract = new Money(moneySubstract.moneyAmount, moneySubstract.moneyPeriod);
        //GameObject.Instantiate(receptionRoom, receptionSpawnPoint);
        
    }
    private void Start()
    {
        //money.AddMoney(moneyAdded); 
        money.AddMoney(money);
        overallMoneyForMinute.AddMoney(overallMoneyForMinute);
        SpawnReception();
    }
    private void Update()
    {
        
       if (desiredHospitalLevel != currentHospitalLevel)
        {
            currentHospitalLevel = desiredHospitalLevel;
            ChangeHospitalLevel(currentHospitalLevel);

        }
        
         /*if (desiredParkingLevel != currentParkingLevel)
         {
             currentParkingLevel = desiredParkingLevel;
             ChangeParkingLevel(currentParkingLevel);

         }*/

        if (desiredCityLevel != currentCityLevel)
        {
            currentCityLevel = desiredCityLevel;
            ChangeCityLevel(currentCityLevel);

        }
        //iCanBuy = money.IsEnough(moneySubstract);
        //if (iCanBuy && buy)
        //{
        //    money.SubstractMoney(moneySubstract);
        //}
    }
    void SpawnReception()
    {
        receptionInstance = GameObject.Instantiate(receptionRoom, receptionSpawnPoint);
    }
    void ChangeCityLevel(int level)
    {
        if (currentCityGO != null)
        {
            Destroy(currentCityGO.gameObject);
        }
        int levelToIIndex = level - 1;
        for (int i = 0; i < levelCityGO.Length; i++)
        {
            if (i == levelToIIndex)
            {
               currentCityGO = GameObject.Instantiate(levelCityGO[i]);
            }
        }
        CityData currentCityData = currentCityGO.GetComponent<CityData>();
        Camera.main.GetComponent<MoveCamera>().SetWorldLimits(currentCityData.limitZForward, currentCityData.limitZBackward, currentCityData.limitXRight, currentCityData.limitXLeft, currentCityData.limitYUp, currentCityData.limitYDown);
    }
    public void ChangeHospitalLevel(int level)
    {
        if (currentHospitalGO != null)
        {
            Destroy(currentHospitalGO.gameObject);
        }
        int levelToIndex = level - 1;
        for (int i = 0; i < levelHospitalGO.Length; i++)
        {
            if (i == levelToIndex)
            {
                currentHospitalGO = GameObject.Instantiate(levelHospitalGO[i]);
                GameObject.Instantiate(hospitalRoomsPrefabs[i], hospitalRoomsSpawnPoints[i]);
                
            }           
        }
        
    }
    public void ChangeParkingLevel(int level)
    {
        GameObject oldParking = currentParkingGO;

        if (currentParkingGO != null)
        {
            oldParking.SetActive(false);

        }

        int levelToIndex = level - 1;
        for (int i = 0; i < levelParkingGO.Length; i++)
        {
            if (i == levelToIndex)
            {
                currentParkingGO = GameObject.Instantiate(CarParkingManager.instance.upgrades[i]);
                CarParkingManager.instance.currentParking = currentParkingGO;
            }
        }

        Destroy(oldParking);


    }
}
