using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainPanelController : MonoBehaviour
{
    TMP_Text moneyForMinute;
    TMP_Text currentMoney;
    TMP_Text currentDiamonds;
    void Start()
    {
        moneyForMinute = transform.GetComponentInChildren<UIMoneyForMinute>().GetComponent<TMP_Text>();
        currentMoney = transform.GetComponentInChildren<UICurrentMoney>().GetComponent<TMP_Text>();
        currentDiamonds = transform.GetComponentInChildren<UICurrentDiamonds>().GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        currentMoney.text = GameData.currentData.money.shortValue;
        currentDiamonds.text = GameData.currentData.currentDiamonds.ToString(); 
        moneyForMinute.text = GameData.currentData.overallMoneyForMinute.shortValue+" /min";
    }
}
