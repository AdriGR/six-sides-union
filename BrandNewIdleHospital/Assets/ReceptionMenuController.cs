using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ReceptionMenuController : MonoBehaviour
{
    public ReceptionController reception;
    public GameObject cashierInfoPrefab;
    TMP_Text currentCashiers;
    public GameObject addCashierPrefab;
    public Button addEmployeeButton;
    public GameObject[] upgradeEmployeeButtons;
    TMP_Text addCashierMoneyText;
    Button addCashierButton;
    public Transform[] buttonPosition;
    public RectTransform buttonsParent;
    public void Initialize(ReceptionController currentReception)
    {
        reception = currentReception; 
        currentCashiers = transform.GetComponentInChildren<UICurrentCashiers>().GetComponent<TMP_Text>();
        
        //addCashierMoneyText = transform.GetComponentInChildren<UIAddRoomAtendantMoney>().GetComponent<TMP_Text>();
        //addCashierButton = transform.GetComponentInChildren<UIAddRoomAttendantButton>().GetComponent<Button>();
        AddCashiersButtons(reception.currentCashiers);
    }
    void AddCashiersButtons(int currentCashiers)
    {
        upgradeEmployeeButtons = new GameObject[currentCashiers];
        for (int i = 0; i < reception.currentCashiers; i++)
        {
            GameObject cashierInfoInstance = GameObject.Instantiate(cashierInfoPrefab, buttonPosition[i]);
            upgradeEmployeeButtons[i] = cashierInfoInstance;
            cashierInfoInstance.GetComponent<UIUpgradeCashierButton>().myCashierIndex = i;
            cashierInfoInstance.GetComponent<UIUpgradeCashierButton>().reception = reception;
        }
        if (reception.currentCashiers < reception.cashiers.Length)
        {
            if (reception.currentCashiers < buttonPosition.Length)
            {
                GameObject addCashierInstance = GameObject.Instantiate(addCashierPrefab, buttonPosition[reception.currentCashiers]);
                addEmployeeButton = addCashierInstance.GetComponentInChildren<Button>();
                addEmployeeButton.onClick.AddListener(delegate { AddEmployee(); });
                addCashierMoneyText = addCashierInstance.transform.GetComponentInChildren<UIAddRoomAtendantMoney>().GetComponent<TMP_Text>();
            }
        }
        
        buttonsParent.sizeDelta = new Vector2(100, 350 * (currentCashiers + 1));
    }
    // Update is called once per frame
    void Update()
    {
        if (reception != null)
        {
            currentCashiers.text = reception.currentCashiers.ToString() + "/" + reception.cashiers.Length.ToString();

            if (addCashierMoneyText != null)
            {
                addCashierMoneyText.text = reception.addCashierCurrentCost.shortValue;
            }
        }
        
        
        
        
    }
    public void AddEmployee()
    {
        if (reception.currentCashiers < reception.cashiers.Length)
        {
            if (GameData.currentData.money.IsEnough(reception.addCashierCurrentCost))
            {
                GameData.currentData.money.SubstractMoney(reception.addCashierCurrentCost);
                reception.AddCashier();
                AddCashiersButtons(reception.currentCashiers);
            }
        }
        
        
    }
    public void UpgradeCashierSpeed(int cashier)
    {
        
        reception.cashiers[cashier].UpgradeSpeed();
    }
    public void BackButton()
    {
        GetComponent<Canvas>().enabled = false;
    }
}
