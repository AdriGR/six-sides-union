﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DeskMenuControl : MonoBehaviour
{
    public RoomController myCounterDesk;
    public void CloseMenu()
    {
        Transform container = transform.Find("Container");
        StartCoroutine(MenuCloseAnimation(container));
    }
    IEnumerator MenuCloseAnimation(Transform menu)
    {
        Tween menuAnim = menu.DOMoveX(menu.position.x - 1000, .3f).SetEase(Ease.InCubic);
        yield return menuAnim.WaitForCompletion();
        myCounterDesk.canvas.enabled = false;
        //Destroy(myCounterDesk.menuInstance);
        //Destroy(menu.gameObject);
    }
}
