using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Money 
{
    public string shortValue;
   
    public double moneyAmount;
    string period;
    
    [HideInInspector]
    public string[] periods =
        {"", "k","M", "B","T", "q", "Q", "s", "S", "o", "N", "d", "U", "D", "Td",
    "qd", "Qd", "sd", "Sd", "Od", "Nd", "V", "Uv", "Dv", "Tv", "qv", "sv", "Sv", "Ov", "Nv", "Tg",
    "Ut", "Dt", "Tt", "qt", "Qt", "st", "St", "Ot", "Nt" };
    //public Money(double amount, string period)
    //{
    //    moneyAmount = amount;
    //    moneyPeriod = period;
    //    ParseMoney();

    //}
    //public Money(float amount, string period)
    //{
    //    moneyAmount = amount;
    //    moneyPeriod = period;
    //    ParseMoney();

    //}
    public Money(double amount)
    {
        moneyAmount = amount;
        shortValue = GetShortValue(amount);
        

    }

    public Money()
    {
        moneyAmount = 0;
        shortValue = GetShortValue(moneyAmount);
        
    }
    string GetShortValue(double amount)
    {
        string result = "";
        for (int i = 0; i < periods.Length; i++)
        {
            if (i > 0)
            {
                double divided = amount / Math.Pow(10, i * 3);
                if (divided < 1)
                {
                    period = periods[i - 1];
                    break;
                }
            }
            else
            {
                period = periods[i];
            }
        }
        string TempString = amount.ToString();
        string tempString = String.Format("{0:0,0}", amount);
        string[] strings = tempString.Split('.');
        string tempResult = "";
        if (strings.Length == 1)
        {
            tempResult = strings[0];
        }
        else if (strings.Length > 1)
        {
            tempResult = String.Concat(strings[0], ".", strings[1]);
        }
        result = String.Concat(tempResult, " ", period);
        return result;
    }
    
    public void AddMoney(Money amountB)
    {
        moneyAmount += amountB.moneyAmount;
        shortValue = GetShortValue(moneyAmount);

    }
    public void SubstractMoney(Money amountB)
    {
        moneyAmount -= amountB.moneyAmount;
        shortValue = GetShortValue(moneyAmount);
    }

    public bool IsEnough(Money otherMoney)
    {
        if (moneyAmount >= otherMoney.moneyAmount)
        {
            return true;
        }
        else 
        {
            return false;
        }       
    }
    

}
