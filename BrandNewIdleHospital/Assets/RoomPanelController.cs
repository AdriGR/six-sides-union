using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomPanelController : MonoBehaviour
{
    public RoomData currentRoom;
    TMP_Text currentRoomName;
    Image currentRoomImage;
    TMP_Text currentRoomLevel;
    TMP_Text upgradeRoomLevel;
    TMP_Text addAttendant;
    TMP_Text upgradeRoomSpeed;
    Button upgradeRoomLevelButton;
    Button addRoomAttendantButton;
    Button upgradeRoomSpeedLevelButton;
    TMP_Text currentIncomeText;
    public void Initialize(RoomData myRoom)
    {
        currentRoom = myRoom;
        currentRoomName = transform.GetComponentInChildren<UIRoomName>().GetComponent<TMP_Text>();
        currentRoomImage = transform.GetComponentInChildren<UIRoomImage>().GetComponent<Image>();
        currentRoomLevel = transform.GetComponentInChildren<UICurrentRoomLevel>().GetComponent<TMP_Text>();
        upgradeRoomLevelButton = transform.GetComponentInChildren<UIUpgradeRoomLevelButton>().GetComponent<Button>();
        upgradeRoomLevel = transform.GetComponentInChildren<UIUpgradeRoomLevelMoney>().GetComponent<TMP_Text>();
        addAttendant = transform.GetComponentInChildren<UIAddRoomAtendantMoney>().GetComponent<TMP_Text>();
        addRoomAttendantButton = transform.GetComponentInChildren<UIAddRoomAttendantButton>().GetComponent<Button>();
        upgradeRoomSpeed = transform.GetComponentInChildren<UIUpgradeRoomSpeed>().GetComponent<TMP_Text>();
        upgradeRoomSpeedLevelButton = transform.GetComponentInChildren<UIUpgradeRoomSpeedButton>().GetComponent<Button>();
        currentIncomeText = transform.GetComponentInChildren<UICurrentIncomeText>().GetComponent<TMP_Text>();
    }

    void Update()
    {
        if (currentRoom != null)
        {
            currentRoomName.text = currentRoom.roomName;
            currentRoomImage.sprite = currentRoom.roomImage;
            currentRoomLevel.text = currentRoom.currentLevel.ToString();
            upgradeRoomLevel.text = currentRoom.currentNextLevelCost.shortValue;
            if (GameData.currentData.money.IsEnough(currentRoom.currentNextLevelCost))
            {
                upgradeRoomLevelButton.interactable = true;
                //upgradeRoomLevelButton.GetComponent<Image>().color = Color.blue;
            }
            else
            {
                upgradeRoomLevelButton.interactable = false;
                //upgradeRoomLevelButton.GetComponent<Image>().color = Color.grey;
            }
            addAttendant.text = currentRoom.currentAttendantsAmountUpdate.shortValue;
            if (GameData.currentData.money.IsEnough(currentRoom.currentAttendantsAmountUpdate))
            {
                addRoomAttendantButton.interactable = true;
                //addRoomAttendantButton.GetComponent<Image>().color = Color.blue;
            }
            else
            {
                addRoomAttendantButton.interactable = false;
                //addRoomAttendantButton.GetComponent<Image>().color = Color.grey;
            }
            upgradeRoomSpeed.text = currentRoom.currentNextAttendSpeedUpdateCost.shortValue;
            if (GameData.currentData.money.IsEnough(currentRoom.currentNextAttendSpeedUpdateCost))
            {
                upgradeRoomSpeedLevelButton.interactable = true;
                //upgradeRoomSpeedLevelButton.GetComponent<Image>().color = Color.blue;
            }
            else
            {
                upgradeRoomSpeedLevelButton.interactable = false;
                //upgradeRoomSpeedLevelButton.GetComponent<Image>().color = Color.grey;
            }
            currentIncomeText.text = currentRoom.currentIncome.shortValue;
        }
        
    }
    public void UpgradeRoomButton()
    {
        
        currentRoom.UpdateLevelTo(currentRoom.currentLevel + 1);
    }
    public void AddRoomAttendantButton()
    {
        currentRoom.AddAttendant();
    }
    public void UpgradeRoomSpeedButton()
    {
        currentRoom.SpeedLevelUp();
    }
    public void BackButton()
    {
        GetComponent<Canvas>().enabled = false;
    }
}
