using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIUpgradeCashierButton : MonoBehaviour
{
    public int myCashierIndex;
    public ReceptionController reception;
    public TMP_Text cashierIndexText;
    public TMP_Text customersPerMinute; 
    public TMP_Text nextUpdateCost;
   
    void Update()
    {
        if (reception != null)
        {
            cashierIndexText.text = (myCashierIndex + 1).ToString();
            customersPerMinute.text = reception.cashiers[myCashierIndex].customersPerMinute.ToString() + "/min";
            nextUpdateCost.text = reception.cashiers[myCashierIndex].currentNextCashierSpeedUpdateCost.shortValue;
        }
    }
    public void UpgradeCashier()
    {
        
        reception.cashiers[myCashierIndex].UpgradeSpeed();
    }
}
